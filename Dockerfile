FROM node:14.17.3 AS front_builder

WORKDIR /usr/src/app

COPY ./client/package*.json ./

RUN npm install

COPY ./client/*.json ./

COPY ./client/src ./src
COPY ./client/public ./public
COPY ./client/vue.config.js ./vue.config.js

RUN npm run build

FROM node:14.17.3 AS development

WORKDIR /usr/src/app

COPY ./server/package*.json ./

RUN npm install

#to use the nest cli
RUN npm install -g @nestjs/cli

#To preserve in cache the previous install layer if any files OTHER than package*.json changed.
#BUT avoid to copy the node_module folder from host to container...
COPY ./server/*.json ./
COPY ./server/api_doc_sdk ./api_doc_sdk
COPY ./server/src ./src
COPY --from=front_builder /usr/src/app/dist ./singlePageApp
#øRUN npm run build


#TODO: uncomment when going to prod...
# ### SECOND STAGE BUILD ### with only the dist/ folder and the production dependencies...
# FROM node:14.17.3 as production

# ARG NODE_ENV=production
# ENV NODE_ENV=${NODE_ENV}

# WORKDIR /usr/src/app

# COPY ./server/package*.json ./

# #rimraf is an executable that is used to clean the installed node packages in a node based project
# #install with only the production dependencies (no more typescript etc...)
# RUN npm install glob rimraf --only=production

# COPY ./server/*.json ./
# COPY ./server/api_doc_sdk ./
# COPY ./server/uploadstore ./
# COPY ./server/src ./

# COPY --from=development /usr/src/app/dist ./dist

# EXPOSE 3000
# EXPOSE 8080
# CMD [ "npm", "run", "start:prod" ]