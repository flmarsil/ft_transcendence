import { Logger, Injectable, CACHE_MANAGER, Inject } from "@nestjs/common";
import {
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from "@nestjs/websockets";
import { Server, Socket } from "socket.io";
import { CreatePongDto } from "./dto/pong.dto";
import { Cache } from "cache-manager";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UserSession } from "../userSessions/entities/userSession.entity";
import { User } from "../users/entities/user.entity";
import { arrayRemove, sleep } from "../utils-dto/utils.dto";
import { GameInstance } from "./gameInstance.class";
import { GameHistoryGameType } from "../gameHistory/entities/gameHistory.entity";
import { Game } from "../games/entities/game.entity";
import { Challenge } from "../games/entities/challenge.entity";

let listequeueClassique = [];
let listequeueBeach = [];
let listequeueFlash = [];

@WebSocketGateway({
  cors: {
    origin: ["http://localhost:3454", "http://localhost:8080"],
    methods: ["GET", "POST", "PUT", "DELETE"],
    allowedHeaders: [
      "X-Requested-With",
      "X-HTTP-Method-Override",
      "Content-Type",
      "Accept",
      "Observe",
      "Content-Length",
      "Accept-Encoding",
      "X-CSRF-Token",
      "Authorization",
    ],
    credentials: true,
  },
}) //{ transports: ['websocket'] })
export class PongGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    @InjectRepository(UserSession)
    private userSessionRepository: Repository<[UserSession, User]>,
    // private userRepository: Repository<User>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) { }

  async isConnected(
    socket: Socket,
    socketId: string,
    str: string
  ): Promise<number> {
    var cookie = require("cookie");
    var cookies = cookie.parse(str);

    let data;
    try {
      data = await JSON.parse(cookies["ft_transcendence_cookie"]);
    } catch (error) {
      //console.log("Json error :", error);
      return -1;
    }

    if (data.login === undefined || data.token === undefined) {
      return -2;
    }

    const cachelogin = await this.cacheManager.get(socketId);

    const user = await User.findOne({
      where: { login: data.login },
    });

    if (user === undefined) {
      return -5;
    }

    if (process.env.FT_AUTHENTICATION === String("false")) {
      if (data.hasOwnProperty("login") === true) {
        this.cacheManager.set(socketId, data.login);
        socket.join(user.username);

        await User.createQueryBuilder()
          .update()
          .set({ status: 1 })
          .where("login=:login ", { login: data.login as string })
          .execute()
          .catch((err) => {
            //console.log("\x1b[36m%s\x1b[0m", err);
          });

        return 1;
      }
      return -20;
    }

    if (cachelogin === data.login) {
      //console.log("user authenticated :", data.login);
      socket.join(user.username);
      return 1;
    }

    let result = await UserSession.findOne({
      where: [{ login: data.login, token: data.token }],
    });

    if (result == undefined) {
      return -4;
    }

    this.cacheManager.set(socketId, result.login);

    await User.createQueryBuilder()
      .update()
      .set({ status: 1 })
      .where("login=:login ", { login: result.login as string })
      .execute()
      .catch((err) => {
        //console.log("\x1b[36m%s\x1b[0m", err);
      });

    //console.log(`\x1b[32mUser connected:    \x1b[0m${result.login}`);
    socket.join(user.username);

    await User.createQueryBuilder()
      .update()
      .set({ status: 1 })
      .where("login=:login ", { login: result.login as string })
      .execute()
      .catch((err) => {
        //console.log("\x1b[36m%s\x1b[0m", err);
      });

    return 1;
  }

  async match(
    player1: string,
    player2: string,
    mapName: string,
    gameType: GameHistoryGameType
  ) {
    //console.log("match : ", player1, "vs", player2);

    let crypto = require("crypto");

    const gameId = await crypto.randomBytes(16).toString("hex");

    const player1_login = await this.cacheManager.get(player1);
    const player2_login = await this.cacheManager.get(player2);

    const User_player1 = await User.findOne({
      where: { login: player1_login },
    });

    const User_player2 = await User.findOne({
      where: { login: player2_login },
    });

    const opponent1 = User_player1.username;
    const opponent2 = User_player2.username;

    const gameInstance = new GameInstance(
      gameId,
      opponent1,
      opponent2,
      this.server.sockets.sockets.get(player1),
      this.server.sockets.sockets.get(player2),
      this.server,
      gameType,
      mapName
    );
  }

  async matchmaking(mapName: string) {
    if (mapName === "classic") {
      let player1 = undefined;
      let player2 = undefined;
      /*console.log(
      "----------- recherche d'une game, queue :",
        listequeueClassique.length
      );*/

      while (listequeueClassique.length >= 2) {
        if (player1 === undefined) {
          player1 = listequeueClassique.shift();
        }
        if (player2 === undefined) {
          player2 = listequeueClassique.shift();
        }

        if (player1 != undefined && player2 != undefined) {
          this.match(player1, player2, mapName, GameHistoryGameType.LADDERGAME);
        }
      }
    } else if (mapName === "beach-volley") {
      let player1 = undefined;
      let player2 = undefined;
      /*console.log(
      "----------- recherche d'une game, queue :",
        listequeueBeach.length
      );*/

      while (listequeueBeach.length >= 2) {
        if (player1 === undefined) {
          player1 = listequeueBeach.shift();
        }
        if (player2 === undefined) {
          player2 = listequeueBeach.shift();
        }

        if (player1 != undefined && player2 != undefined) {
          this.match(player1, player2, mapName, GameHistoryGameType.LADDERGAME);
        }
      }
    } else if (mapName === "light") {
      let player1 = undefined;
      let player2 = undefined;
      /*console.log(
      "----------- recherche d'une game, queue :",
        listequeueFlash.length
      );*/

      while (listequeueFlash.length >= 2) {
        if (player1 === undefined) {
          player1 = listequeueFlash.shift();
        }
        if (player2 === undefined) {
          player2 = listequeueFlash.shift();
        }

        if (player1 != undefined && player2 != undefined) {
          this.match(player1, player2, mapName, GameHistoryGameType.LADDERGAME);
        }
      }
    }
  }

  async listequeueDeleteSocketID(socketID: string) {
    listequeueClassique = arrayRemove(listequeueClassique, socketID);
    listequeueBeach = arrayRemove(listequeueBeach, socketID);
    listequeueFlash = arrayRemove(listequeueFlash, socketID);
  }

  private logger = new Logger("PongGateway");

  async afterInit(server: Server) {
    this.logger.log("Initialised");
    server.use(async (socket, next) => {
      let ret = 0;
      if (
        (ret = await this.isConnected(
          socket,
          socket.id,
          socket.handshake.headers.cookie
        )) != 1
      ) {
        let err;
        //console.log("Error code :", ret);
        if (ret == -1 || ret == -2) {
          err = new Error("{ error: 'Bad Request' }");
          next(err);
        } else if (ret == -3 || ret == -4) {
          err = new Error("{ error: 'Unauthorized, user not connected' }");
          next(err);
        }
      }
      next();
    });
  }

  async handleConnection(client: Socket) {
    /*console.log(
    `\x1b[32mclient connected to pongGateway:    \x1b[0m${client.id}`
    );*/
    client.use(async (socket, next) => {
      let ret = 0;
      if (
        (ret = await this.isConnected(
          client,
          client.id,
          client.handshake.headers.cookie
        )) != 1
      ) {
        let err;
        if (ret == -1 || ret == -2) {
          err = new Error("{ error: 'Bad Request' }");
          next(err);
        } else if (ret == -3 || ret == -4) {
          err = new Error("{ error: 'Unauthorized, user not connected' }");
          next(err);
        }
      }
      next();
    });
  }

  async handleDisconnect(client: Socket) {
    //console.log(`\x1b[31mclient disconnected from pong: \x1b[0m${client.id}`);
    const cachelogin = await this.cacheManager.get(client.id);

    if (cachelogin != undefined) {
      await this.cacheManager.del(client.id);
      await User.createQueryBuilder()
        .update()
        .set({ status: 0 })
        .where("login=:login ", { login: cachelogin as string })
        .execute()
        .catch((err) => {
          //console.log("\x1b[36m%s\x1b[0m", err);
        });
    }
  }

  //note: this is required when we want to answer to all clients at once (not just one...)
  @WebSocketServer()
  server: Server;

  @SubscribeMessage("findGame")
  handleFindGameRequest(client: Socket, text: CreatePongDto) {
    //console.log("findGame request, map:", text.data);

    client.on("notWaiting", (...args) => {
      this.listequeueDeleteSocketID(client.id);
    });

    client.on("disconnect", (reason) => {
      this.listequeueDeleteSocketID(client.id);
    });

    if (
      listequeueClassique.indexOf(client.id) != -1 ||
      listequeueBeach.indexOf(client.id) != -1 ||
      listequeueFlash.indexOf(client.id) != -1
    ) {
      client.emit("waitGame", { data: { response: "wait..." } });
      return;
    }

    if (text.data === "classic") {
      listequeueClassique.push(client.id);
      client.emit("waitGame", { data: { response: "wait..." } });
      //console.log(client.id + " mis dans la liste d'attente");
      //console.log("listequeueClassique :", listequeueClassique);
    } else if (text.data === "beach-volley") {
      listequeueBeach.push(client.id);
      client.emit("waitGame", { data: { response: "wait..." } });
      //console.log(client.id + " mis dans la liste d'attente");
      //console.log("listequeueBeach :", listequeueBeach);
    } else if (text.data === "light") {
      listequeueFlash.push(client.id);
      client.emit("waitGame", { data: { response: "wait..." } });
      //console.log(client.id + " mis dans la liste d'attente");
      //console.log("listequeueFlash :", listequeueFlash);
    } else {
      client.emit("error", { data: { response: "you have not chosen a map" } });
      return;
    }

    this.matchmaking(text.data);
  }

  @SubscribeMessage("watchGame")
  async handleWatchGameRequest(client: Socket, text: CreatePongDto) {
    const game = await Game.findOne({
      where: [{ gameID: text.data }],
    });

    if (game != undefined) {
      let data = {
        gameId: game.gameID,
        player1: game.username_player1,
        player2: game.username_player2,
        scorePlayer1: game.score_player1,
        scorePlayer2: game.score_player2,
        mapName: game.mapName,
      };
      client.join(text.data);
      client.emit("watchGameFound", {
        data: data,
      });

      ///TODO:
      client.on("leaveWatchGame", ({ data }) => {
        client.leave(data);
      });
    } else {
      client.emit("error", {
        data: "game not found",
      });
    }
  }

  /*exemple front :  socket.emit("challenge", {
  data: { username: "fassani", mapName: state.nameMap },
});*/
  @SubscribeMessage("challenge")
  async handleChallengeGameRequest(client: Socket, data: any) {
    //console.log("---------- ## new challenge", data);

    const player1_login = await this.cacheManager.get(client.id);

    const User_player1 = await User.findOne({
      where: { login: player1_login },
    });

    const count = await Challenge.count({
      where: { username_player1: User_player1.username },
    });

    if (count != 0) {
      //console.log("error : already have a challenge on the way");
      client.emit("challengeError", {
        data: "you already have a challenge on the way",
      });
      return;
    }

    const username = data.data.username;
    const User_player2 = await User.findOne({
      where: { username: username },
    });

    if (
      User_player2.playingCurrently === true ||
      User_player2.status === 0 ||
      User_player2.status === 2
    ) {
      let erroMessage;

      if (User_player2.playingCurrently === true) {
        erroMessage = `you can't challenge ${User_player2.username} for the moment, ${User_player2.username} is already in play`;
      }

      if (User_player2.status === 0) {
        erroMessage = `you can't challenge ${User_player2.username} for the moment, ${User_player2.username} is offline`;
      }

      if (User_player2.status === 2) {
        erroMessage = `you can't challenge ${User_player2.username} for the moment, ${User_player2.username} is in do not disturb mode`;
      }

      client.emit("challengeError", {
        data: erroMessage,
      });
      return;
    }

    let crypto = require("crypto");
    const challengeID = await crypto.randomBytes(16).toString("hex");

    const challenge = new Challenge();

    challenge.challengeID = challengeID;
    challenge.socketID_player1 = client.id;
    challenge.username_player1 = User_player1.username;
    challenge.username_player2 = username;
    challenge.mapName = data.data.mapName;

    await challenge.save();

    client.emit("waitChallenge");

    this.server.to(username).emit("challengeRequest", { data: challenge });

    setTimeout(
      async function () {
        const count = await Challenge.count({
          where: { challengeID: challenge.challengeID },
        });

        if (count != 0) {
          client.emit("challengeError", {
            data: "expired challenge",
          });
        }

        challenge.remove();
      },
      10000,
      challenge,
      client
    );
  }

  /*exemple front :  socket.emit("acceptChallenge", {
  data: { challengeID: "dfs4sf5fsf45s5fdsdfs"},
});*/
  @SubscribeMessage("acceptChallenge")
  async handleAcceptChallengeGameRequest(client: Socket, data: any) {
    const challenge = await Challenge.findOne({
      where: { challengeID: data.data.challengeID },
    });

    if (challenge != undefined) {
      this.server.sockets.sockets
        .get(challenge.socketID_player1)
        .emit("challengeGameFound");
      client.emit("challengeGameFound");
      await sleep(1000);
      this.match(
        challenge.socketID_player1,
        client.id,
        challenge.mapName,
        GameHistoryGameType.DUELGAME
      );
      challenge.remove();
    } else {
      client.emit("challengeError", {
        data: "expired challenge",
      });
    }
  }

  /*exemple front :  socket.emit("refuseChallenge", {
  data: { challengeID: "dfs4sf5fsf45s5fdsdfs"},
});*/
  @SubscribeMessage("refuseChallenge")
  async handleRefuseChallengeGameRequest(client: Socket, data: any) {
    const challenge = await Challenge.findOne({
      where: { challengeID: data.data.challengeID },
    });

    challenge.remove();

    if (challenge != undefined) {
      this.server.sockets.sockets
        .get(challenge.socketID_player1)
        .emit("challengeError", {
          data: "challenge declined",
        });
    }
  }
}
