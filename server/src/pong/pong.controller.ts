import { Controller, Header, Get, Res, Param, } from '@nestjs/common';

@Controller('api/v1')
export class PongController {
    constructor() { }

    @Get('/pong')
    @Header('Content-Type', 'text/html; charset=utf-8')
    getChat(@Res() res) {
        //console.log(`index.html sent.`);
        return res.sendFile('index.html', { root: './src/pong/' });
    }

    @Get('/pong/:file')
    @Header('Content-Type', 'text/html; charset=utf-8')
    getPongJsFile(@Res() res, @Param('file') file) {
        //console.log(`${file} sent.`);
        return res.sendFile(file, { root: './src/pong/' });
    }

    @Get('/pong/game')
    @Header('Content-Type', 'text/html; charset=utf-8')
    getpong(@Res() res) {
        //console.log(`index.html sent.`);
        return res.sendFile('index.html', { root: './src/pong/game/' });
    }

    @Get('/pong/game/:file')
    @Header('Content-Type', 'text/html; charset=utf-8')
    getpongGameJsFile(@Res() res, @Param('file') file) {
        //console.log(`${file} sent.`);
        return res.sendFile(file, { root: './src/pong/game/' });
    }
}