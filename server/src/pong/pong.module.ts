import { Module, CacheModule } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PongController } from './pong.controller';

@Module({
    imports: [TypeOrmModule.forFeature([]), CacheModule.register({ ttl: 0 }), DatabaseModule, PongModule],
    providers: [],
    controllers: [PongController]
})
export class PongModule { }