export class Map {
  private name_: string;
  private width_: number;
  private height_: number;
  private powersUp_: string[];

  private paddle_marging: number;
  private paddle_width: number;
  private paddle_height: number;
  private size_ball: number;
  private speed_ball: number;
  private player_speed: number;
  private gamePoints: number;

  constructor(private nameMap: string) {
    this.name_ = undefined;
    this.width_ = 1400;
    this.height_ = 1000;
    this.powersUp_ = [];

    this.paddle_marging = 150;
    this.paddle_width = 18;
    this.paddle_height = 80;
    this.size_ball = 25;
    this.speed_ball = 12;
    this.player_speed = 20;
    this.gamePoints = 11;

    this.setMap(this.nameMap)
  }

  getName(): string {
    return this.name_;
  }
  getWidth(): number {
    return this.width_;
  }
  getHeight(): number {
    return this.height_;
  }
  getPowersUp(): string[] {
    return this.powersUp_;
  }

  getPaddle_marging(): number { return this.paddle_marging };
  getPaddle_width(): number { return this.paddle_width };
  getPaddle_height(): number { return this.paddle_height };
  getSize_ball(): number { return this.size_ball };
  getSpeed_ball(): number { return this.speed_ball };
  getPlayer_speed(): number { return this.player_speed };
  getGamePoints(): number { return this.gamePoints };


  setGamePoints(points: number): number { return this.gamePoints = points };

  setMap(name: string) {
    this.name_ = name;

    //console.log('--------- MAP :', name)

    if (name === "classic") {
      this.resetClassic()
    } else if (name === "beach-volley") {
      this.resetBeach_volley()
    } else if (name === "light") {
      this.resetLight()
    }

  }

  resetClassic() {
    this.paddle_marging = 150;
    this.paddle_width = 18;
    this.paddle_height = 80;
    this.size_ball = 25;
    this.speed_ball = 12;
    this.player_speed = 20;
  }


  resetBeach_volley() {
    this.paddle_marging = 150;
    this.paddle_width = 20;
    this.paddle_height = 20;
    this.size_ball = 35;
    this.speed_ball = 12;
    this.player_speed = 20;
    //this.powersUp_ = ['SpeedyGonzales', 'ReverseDirection', 'ReturnToTheSender', 'SuperSizeMe', 'HazardousDirection']
    this.powersUp_ = ['ReverseDirection', 'ReturnToTheSender', 'SuperSizeMe']
  }

  resetLight() {
    this.paddle_marging = 150;
    this.paddle_width = 9;
    this.paddle_height = 50;
    this.size_ball = 16;
    this.speed_ball = 18;
    this.player_speed = 30;
    // this.powersUp_ = ['BulletTime', 'ReverseDirection', 'ReturnToTheSender', 'AntMan', 'HazardousDirection']
    this.powersUp_ = ['ReverseDirection', 'ReturnToTheSender', 'AntMan']
  }


}
