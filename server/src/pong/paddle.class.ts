
export class Paddle {
    private width_: number
    private height_: number
    private x_: number
    private y_: number
    private speed_: number
    private reverseDirection: boolean

    constructor(private width: number, private height: number, private speed: number) {
        this.width_ = this.width
        this.height_ = this.height
        this.speed_ = this.speed
        this.x_ = 0
        this.y_ = 0
        this.reverseDirection = false
    }

    getWidth(): number { return this.width_ }
    getHeight(): number { return this.height_ }
    getX(): number { return this.x_ }
    getY(): number { return this.y_ }
    getSpeed(): number { return this.speed_ }
    getReverseDirection(): boolean { return this.reverseDirection }

    toObj(): object {
        return {
            'w': this.getWidth(),
            'h': this.getHeight(),
            'x': this.getX(),
            'y': this.getY(),
        }
    }

    setWidth(width: number): number { return this.width_ = width }
    setHeight(height: number): number { return this.height_ = height }
    setX(x: number): number { return this.x_ = x }
    setY(y: number): number { return this.y_ = y }
    setSpeed(speed: number): number { return this.speed_ = speed }
    setReverseDirection(reverse: boolean): boolean { return this.reverseDirection = reverse }

    //paddle effects :
    effect_SuperSizeMe() {
        const that = this
        const oldHeight = this.getHeight()
        this.setHeight(oldHeight + 100)
        setTimeout(function () {
            that.setSpeed(oldHeight)
        }, 3000, that, oldHeight);
    }

    effect_AntMan() {
        const that = this
        const oldHeight = this.getHeight()
        this.setHeight(20)
        setTimeout(function () {
            that.setSpeed(oldHeight)
        }, 1500, that, oldHeight);
    }

    effect_ReverseDirection() {
        const that = this
        this.setReverseDirection(true)
        setTimeout(function () {
            that.setReverseDirection(false)
        }, 1700, that);
    }
}