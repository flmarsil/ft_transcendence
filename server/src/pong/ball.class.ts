import { sleep } from "../utils-dto/utils.dto";
import { DIRECTION } from "./pong_utils.dto";

export class Ball {
  private size_: number;
  private x_: number;
  private y_: number;
  private moveX_: DIRECTION;
  private moveY_: DIRECTION;
  private speed_: number;

  constructor(private size: number, private speed: number) {
    this.size_ = this.size;
    this.speed_ = this.speed;
    this.x_ = 0;
    this.y_ = 0;
    this.moveX_ = DIRECTION.IDLE;
    this.moveY_ = DIRECTION.IDLE;
  }

  getSize(): number {
    return this.size_;
  }
  getX(): number {
    return this.x_;
  }
  getY(): number {
    return this.y_;
  }
  getMoveX(): DIRECTION {
    return this.moveX_;
  }
  getMoveY(): DIRECTION {
    return this.moveY_;
  }
  getSpeed(): number {
    return this.speed_;
  }

  toObj(): object {
    return {
      's': this.getSize(),
      'x': this.getX(),
      'y': this.getY(),
    };
  }

  setSize(size: number): number {
    return (this.size_ = size);
  }
  setX(x: number): number {
    return (this.x_ = x);
  }
  setY(y: number): number {
    return (this.y_ = y);
  }
  setMoveX(moveX: DIRECTION): DIRECTION {
    return (this.moveX_ = moveX);
  }
  setMoveY(moveY: DIRECTION): DIRECTION {
    return (this.moveY_ = moveY);
  }
  setSpeed(speed: number): number {
    return (this.speed_ = speed);
  }

  //ball effects :
  effect_BulletTime() {
    const that = this
    const oldSpeed = this.getSpeed()
    this.setSpeed(2)
    setTimeout(function () {
      that.setSpeed(oldSpeed)
    }, 1000, that, oldSpeed);

  }

  effect_SpeedyGonzales() {
    const that = this
    const oldSpeed = this.getSpeed()
    this.setSpeed(oldSpeed + 15)
    setTimeout(function () {
      that.setSpeed(oldSpeed)
    }, 1000, that, oldSpeed);
  }

  async effect_HazardousDirection() {
    let i = 0
    let changement = await Math.random() * (5 - 2) + 2
    while (i < changement) {
      await sleep(500)

      const direction = await Math.floor(Math.random() * 1)

      if (this.getMoveY() === DIRECTION.DOWN) {
        this.setMoveY(DIRECTION.UP)
        if (direction == 0) {
          this.setMoveX(DIRECTION.RIGHT)
        } else {
          this.setMoveX(DIRECTION.LEFT)
        }
      } else {
        this.setMoveY(DIRECTION.DOWN)
        if (direction == 0) {
          this.setMoveX(DIRECTION.RIGHT)
        } else {
          this.setMoveX(DIRECTION.LEFT)
        }
      }

      i = i + 1
    }

  }

  effect_ReturnToTheSender() {
    if (this.getMoveX() === DIRECTION.LEFT) {
      this.setMoveX(DIRECTION.RIGHT)
    } else {
      this.setMoveX(DIRECTION.LEFT)
    }
    if (this.getMoveY() === DIRECTION.DOWN) {
      this.setMoveY(DIRECTION.UP)
    } else {
      this.setMoveY(DIRECTION.DOWN)
    }

  }
}
//
