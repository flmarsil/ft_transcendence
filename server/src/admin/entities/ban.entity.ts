
import { Entity, CreateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';


@Entity({ name: "bans" })
export class Ban extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    login: string;

    @Column({ unique: false })
    reason: string;


    @CreateDateColumn({})
    createdAt: Date
}