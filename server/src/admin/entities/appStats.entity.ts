
import { Entity, CreateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';


@Entity({ name: "appStats" })
export class AppStats extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: 0 })
    number_of_games_played: number;

    @Column({ default: 0 })
    number_of_users: number;

    @Column({ default: 0 })
    number_of_banned_users: number;

    @CreateDateColumn({})
    date_of_server_launch: Date
}