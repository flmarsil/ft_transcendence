import { CacheModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { AdminService } from './admin.service';
import { AdminController } from './admin.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ban } from './entities/ban.entity';
import { AppStats } from './entities/appStats.entity';
import { User } from '../users/entities/user.entity';
import { AuthentificationMiddleware } from '../middlewares/authentification.middleware';
import { AdminAuthentificationMiddleware } from '../middlewares/adminAuthentification.middleware';

@Module({
    imports: [TypeOrmModule.forFeature([Ban, User, AppStats]), CacheModule.register({ ttl: 0 })],
    exports: [TypeOrmModule],
    controllers: [AdminController],
    providers: [AdminService]
})
export class AdminModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthentificationMiddleware)
            .forRoutes(AdminController);
        consumer
            .apply(AdminAuthentificationMiddleware)
            .forRoutes(AdminController);
    }
}