import { Injectable, Session, CACHE_MANAGER, Inject, Res } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserSession } from '../userSessions/entities/userSession.entity'
import { isAdmin, ResponseOBJ, ResponseStatus, download, uploadstore_deletefile, isUserNameValid, addErrorToResponseObj, addInternalServerErrorToResponseObj, } from '../utils-dto/utils.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity'
import { HttpService } from '@nestjs/axios';
import { Cache } from 'cache-manager';
import { Query } from 'typeorm/driver/Query';
import { DirectMessages } from '../direct-messages/entities/direct-messages.entity';


@Injectable()
export class UsersService {
  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) { }

  async saveSessionWithUser(user: User, tempOBJ: {}) {
    const session = new UserSession();
    session.login = user.login;
    //TODO
    session.token = tempOBJ['token'] as string
    await session.save();
  }

  async doubleFA_auth_check(login: string): Promise<string> {

    const user = await User.findOne({
      where: [
        { 'login': login },
      ]
    })

    if (user.twoFactorAuth === false) {
      return "OK"
    } else if (user.twoFactorAuth === true) {

      //créer le 2fa envoyer le mail

      return "2FA"
    }
  }

  //TODO remove me when done develeoping
  //to create fake account from the outside.
  async TMPcreateAccount(responseObj: {}, ProfileData: {}): Promise<any> {
    await User.count({
      where: [
        { login: ProfileData['login'] },
      ]
    })
      .then(async res => {
        if (res == 0) {
          const user = new User();
          user.login = ProfileData['login'];
          user.email = ProfileData['email'];
          user.username = ProfileData['login'];
          user.avatar = ProfileData['image_url'];
          await user.save().then(res => { })
            .catch(error => {
              responseObj['status'] = 'ERR';
              responseObj['ERR'] = 'internal server error';
              responseObj['ERR_DESCRIPTION'] = 'user could not be saved in database.';
              //console.log('\x1b[36m%s\x1b[0m', error);
            });
        }
        else {
          responseObj['status'] = ResponseStatus.ERROR;
          responseObj[ResponseStatus.ERROR] = 'login taken already';
          //console.log("login existe");
        }
      })
      .catch((err) => {
        //console.log(err)
      });

    return responseObj;
  }


  //créer un compte
  async createAccount(responseObj: ResponseOBJ, ProfileData: {}, tempOBJ: {}): Promise<any> {
    await User.count({
      where: [
        { 'login': ProfileData['login'] as string },
      ]
    })
      .then(async res => {
        if (res == 0) {
          const user = new User();
          user.login = ProfileData['login'] as string
          user.email = ProfileData['email'] as string
          user.username = ProfileData['login'] as string

          let crypto = require("crypto");
          const randomName = crypto.randomBytes(30).toString('hex');
          const login = ProfileData['login'] as string
          let pathAvatar = `${login}_${randomName}.jpg`;
          await download(ProfileData['image_url'], pathAvatar)
          user.avatar = process.env.FT_HOST + "avatar/" + pathAvatar;


          //TODO need to add a couple of parameters so we can add the tokenFromIntra for example...
          this.saveSessionWithUser(user, tempOBJ);
          await user.save().then(res => { })
            .catch((err) => {
              //console.log('\x1b[36m%s\x1b[0m', err);
              addInternalServerErrorToResponseObj(responseObj);
            });
        }
        else {
          await UserSession.createQueryBuilder()
            .update()
            .set({ token: tempOBJ['token'] as string })
            .where('login=:login', {
              'login': ProfileData['login'] as string
            })
            .execute()
          //console.log("login existe");
          //console.log('\x1b[36m%s\x1b[0m', '----------- session saved')
        }
      })
      .catch((err) => {
        //console.log('\x1b[36m%s\x1b[0m', err);
        addInternalServerErrorToResponseObj(responseObj);
      });

    return responseObj;
  }


  async new_createAccount(ProfileData: {}) {
    const count = await User.count({
      where: [
        { 'login': ProfileData['login'] as string },
      ]
    })

    if (count == 0) {
      const user = new User();
      user.login = ProfileData['login'] as string
      user.email = ProfileData['email'] as string
      user.username = ProfileData['login'] as string

      let crypto = require("crypto");
      const randomName = crypto.randomBytes(30).toString('hex');
      const login = ProfileData['login'] as string
      let pathAvatar = `${login}_${randomName}.jpg`;

      await download(ProfileData['image_url'], pathAvatar)

      user.avatar = process.env.FT_HOST + "avatar/" + pathAvatar;
      await user.save()
    } else {
      //console.log("error: new_createAccount l'user existe deja")
    }
  }

  //obtenir les informations d'un compte
  async getAccountInfo(responseObj: ResponseOBJ, query): Promise<string> {
    const user = await User.findOne({
      where: [
        { 'login': query['login'] as string },
      ],
      select: [
        'id',
        'login',
        'username',
        'email',
        'avatar',
        'wins',
        'loses',
        'ratio',
        'xp',
        'status',
        'playingCurrently',
        'twoFactorAuth',
        'createdAt'
      ],
    })

    if (user == undefined) {
      addErrorToResponseObj(responseObj, "requested user does not exist", 0);
    } else {
      if (await isAdmin(user.login) == true) {
        user['isAdmin'] = true
      } else {
        user['isAdmin'] = false
      }
      responseObj.data = user
    }
    return JSON.stringify(responseObj);
  }

  //supprimer un compte
  async disableAccount(@Res() res, responseObj: ResponseOBJ, query) {

    const user = await User.findOne({
      where: [
        { 'login': query['login'] as string },
      ]
    })

    if (user === undefined) {
      addErrorToResponseObj(responseObj, "requested user does not exist", 0);
      res.send(JSON.stringify(responseObj))
      return
    }

    await User.createQueryBuilder() // supprime l'User
      .delete()
      .where('id =:id ', { id: user.id })
      .execute()
      .catch((err) => {
        //console.log('\x1b[36m%s\x1b[0m', err);
        addInternalServerErrorToResponseObj(responseObj);
        res.send(JSON.stringify(responseObj))
        return
      });

    await uploadstore_deletefile(user.avatar) // supprime l'avatar de l'User
    await this.logoutAccount(res, responseObj, query)
  }
  //se déconnecter d'un compte
  async logoutAccount(@Res() res, responseObj: ResponseOBJ, query) {

    await this.cacheManager.del(query['login'] as string) //supprime l'User du cache

    await UserSession.createQueryBuilder() // supprime la Session de l'User
      .delete()
      .where('login =:login ', { login: query['login'] as string })
      .execute()
      .catch((err) => {
        //console.log('\x1b[36m%s\x1b[0m', err);
        addInternalServerErrorToResponseObj(responseObj);
      });
    //res.cookie('ft_transcendence_cookie', '?') //supprime le login et token du cookie
    res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
    res.send(JSON.stringify(responseObj))

  }

  //changer l'username d'un compte
  async changeUsername(responseObj: ResponseOBJ, query): Promise<string> {

    if (!await isUserNameValid(query['username'] as string)) {
      addErrorToResponseObj(responseObj, "username is not valid, a username can only contain : Lowercase Letters, Numbers, Dots, Underscores and No more than 15 characters", 0);
      return JSON.stringify(responseObj)
    }

    const countUser = await User.count({
      where: [
        { 'username': query['username'] as string },
      ]
    })

    if (query['username'] as string == "Serveur") {
      addErrorToResponseObj(responseObj, "you are not allowed to take this username", 2);
      return JSON.stringify(responseObj)
    }

    if (countUser != 0) {
      addErrorToResponseObj(responseObj, "username already used", 1);
      return JSON.stringify(responseObj)
    }

    const user = await User.findOne({
      where: [
        { 'login': query['login'] as string },
      ]
    })


    let loginUsername = user.username


    await User.createQueryBuilder()
      .update()
      .set({ username: query['username'] })
      .where('login=:login ', { 'login': query['login'] as string })
      .execute()
      .catch((err) => {
        //console.log('\x1b[36m%s\x1b[0m', err);
        addInternalServerErrorToResponseObj(responseObj);
      });

    /////FASSANI !!!!!!



    await DirectMessages.createQueryBuilder()
      .update()
      .set({ 'emitterUsername': query['username'] })
      .where('emitterUsername=:username ', { 'username': loginUsername })
      .execute()
      .catch((err) => {
        //console.log('\x1b[36m%s\x1b[0m', err);
        addInternalServerErrorToResponseObj(responseObj);
      });



    return JSON.stringify(responseObj);
  }


  //changer le status d'un compte
  async changeUserStatus(responseObj: ResponseOBJ, query): Promise<string> {

    const tmpstatus = query['status'] as number

    if ((tmpstatus < 0) || (tmpstatus > 2)) {
      addErrorToResponseObj(responseObj, "requested status is not valid, it must be between 0 and 2 inclusive, 0: offline, 1: online, 2: do not disturb", 0);
      return JSON.stringify(responseObj)
    }

    await User.createQueryBuilder()
      .update()
      .set({ status: tmpstatus })
      .where('login=:login ', { 'login': query['login'] as string })
      .execute()
      .catch((err) => {
        //console.log('\x1b[36m%s\x1b[0m', err);
        addInternalServerErrorToResponseObj(responseObj);
      });

    return JSON.stringify(responseObj);
  }


  //changer la double authentification d'un compte
  async twoFactorAuth(responseObj: ResponseOBJ, query): Promise<string> {

    let tmpbool: boolean;

    if (query['activate'] === 1 || query['activate'] === 'true' || query['activate'] === true) {
      tmpbool = true
    } else if (query['activate'] === 0 || query['activate'] === 'false' || query['activate'] === false) {
      tmpbool = false
    } else {
      addErrorToResponseObj(responseObj, "value sent in the query activate must be a boolean", 0);
      return JSON.stringify(responseObj)
    }

    await User.createQueryBuilder()
      .update()
      .set({ twoFactorAuth: tmpbool })
      .where('login=:login ', { 'login': query['login'] as string })
      .execute()
      .catch((err) => {
        //console.log('\x1b[36m%s\x1b[0m', err);
        addInternalServerErrorToResponseObj(responseObj);
      });

    return JSON.stringify(responseObj);
  }

  //changer l'avatar d'un compte
  async changeAccountAvatar(responseObj: ResponseOBJ, query, filename: string): Promise<string> {

    let user = await User.findOne({
      where: [
        { 'login': query['login'] as string },
      ]
    })

    await uploadstore_deletefile(user.avatar) //permet de supprimer l'ancien avatar

    user.avatar = process.env.FT_HOST + "avatar/" + filename  // nouveau avatar

    await user.save()
    responseObj.data = { 'avatar': user.avatar }
    return JSON.stringify(responseObj);
  }
}
