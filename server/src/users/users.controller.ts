import { Controller, Get, Post, Body, Patch, Param, Delete, Query, UseInterceptors, UploadedFile, Res, } from '@nestjs/common';
import { UsersService } from './users.service';
import { initResponse, editFileName, imageFileFilter, addErrorToResponseObj, ResponseOBJ } from '../utils-dto/utils.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

//TODO change this..
@Controller('api/v1/user')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  //TODO delete me when done developing.
  //To create fake accounts from the outside.
  @Post()
  async createAccount(@Query() query): Promise<string> {
    return await this.usersService.TMPcreateAccount(initResponse(query), query);
  }

  @Get()
  async getAccountInfo(@Query() query): Promise<string> {
    return await this.usersService.getAccountInfo(initResponse(query), query);
  }

  @Delete()
  async disableAccount(@Res() res, @Query() query) {
    await this.usersService.disableAccount(res, initResponse(query), query);
  }

  @Get('/logout')
  async logoutAccount(@Res() res, @Query() query) {
    await this.usersService.logoutAccount(res, initResponse(query), query);
  }

  @Post('/username')
  async changeUsername(@Query() query): Promise<string> {
    return await this.usersService.changeUsername(initResponse(query), query);
  }

  @Post('/status')
  async changeUserStatus(@Query() query): Promise<string> {
    return await this.usersService.changeUserStatus(initResponse(query), query);
  }

  @Post('/twoFactorAuth')
  async twoFactorAuth(@Query() query): Promise<string> {
    return await this.usersService.twoFactorAuth(initResponse(query), query);
  }

  @Post('/avatar')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploadstore',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async changeAccountAvatar(@UploadedFile() file, @Query() query): Promise<string> {
    if (query['errorFile'] == 'error') {
      let responseObj: ResponseOBJ = initResponse(query)
      addErrorToResponseObj(responseObj, "Only .JPG image file are allowed!.", 0);
      return JSON.stringify(responseObj)
    }

    //console.log("\x1b[44m%s\x1b[0m", "Avatar file uploaded :", file.filename);
    return this.usersService.changeAccountAvatar(initResponse(query), query, file.filename);
  }

}

