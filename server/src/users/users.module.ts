import { CacheModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity'
import { AuthentificationMiddleware } from '../middlewares/authentification.middleware'
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [TypeOrmModule.forFeature([User]), CacheModule.register({ ttl: 0 })],
  exports: [TypeOrmModule],
  controllers: [UsersController],
  providers: [UsersService]
})

export class UsersModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthentificationMiddleware)
      .forRoutes(UsersController);
  }
}
