
import { Entity, CreateDateColumn, OneToMany, PrimaryGeneratedColumn, Column, BaseEntity, ManyToMany, Double } from 'typeorm';
import { ChatChannel } from '../../chatChannels/entities/chatChannel.entity';
import { ChatChannelBannedOrMutedUser } from '../../chatChannels/entities/chatChannelBannedOrMutedUser.entity';
import { DirectMessageThreads } from '../../direct-messages/entities/direct-message-threads.entity';
import { GameHistory } from '../../gameHistory/entities/gameHistory.entity';


@Entity({ name: "users" })
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    login: string;

    @Column({ unique: true })
    username: string;

    @Column({ unique: true })
    email: string;

    @Column()
    avatar: string;

    @Column({ default: 0 })
    wins: number;

    @Column({ default: 0 })
    loses: number;

    @Column({ type: 'real', default: '0' })
    ratio: string;

    @Column({ default: 0 })
    xp: number;

    @Column({ default: 0 })
    lvl: number;

    //0: offline, 1: online, 2: do not disturb
    @Column({ default: 0 })
    status: number;

    //0: offline, 1: online, 2: do not disturb
    @Column({ default: false })
    playingCurrently: boolean;

    @Column({ default: false })
    twoFactorAuth: boolean;

    @CreateDateColumn({})
    createdAt: Date

    //relation bidirectional, recessary to facilitate the leftJoin
    @ManyToMany((type) => ChatChannel, (chatChannel) => chatChannel.admins,
        {
            onDelete: 'CASCADE' //if an admin is deleted, the entry in table chatChannelId_adminId is deleted.
        })
    chatChannelsAsAdmin: ChatChannel[];

    //relation bidirectional, recessary to facilitate the leftJoin
    @ManyToMany((type) => ChatChannel, (chatChannel) => chatChannel.guests,
        {
            onDelete: 'CASCADE' //if an guest is deleted, the entry in table chatChannelId_guestId is deleted.
        })
    chatChannelsAsGuest: ChatChannel[];

    @OneToMany((type) => ChatChannelBannedOrMutedUser, (chatChannelBannedOrMutedUsers) => chatChannelBannedOrMutedUsers.user)
    chatChannelBannedOrMutedUsers: ChatChannelBannedOrMutedUser[];

    //gameHistory
    @OneToMany((type) => GameHistory, (gameHistory) => gameHistory.loser)
    gameHistoryAsLoser: GameHistory[];

    //gameHistory
    @OneToMany((type) => GameHistory, (gameHistory) => gameHistory.winner)
    gameHistoryAsWinner: GameHistory[];

    //directMessageThreads
    @OneToMany((type) => DirectMessageThreads, (directMessageThread) => directMessageThread.user1)
    directMessageThreadAsUser1: DirectMessageThreads[];

    //directMessageThreads
    @OneToMany((type) => DirectMessageThreads, (directMessageThread) => directMessageThread.user2)
    directMessageThreadAsUser2: DirectMessageThreads[];

}