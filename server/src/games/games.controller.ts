import { Controller, Get, Post, Query, } from '@nestjs/common';
import { GamesService } from './games.service';
import { initResponse } from '../utils-dto/utils.dto';

@Controller('/api/v1')
export class GamesController {
  constructor(private readonly gamesService: GamesService) { }

  //game
  @Get('/game/findAll')
  async findAllGame(@Query() query) {
    return await this.gamesService.findAllGame(initResponse(query), query);
  }

  @Get('/game/findOne')
  async findOneGame(@Query() query) {
    return await this.gamesService.findOneGame(initResponse(query), query);
  }

}
