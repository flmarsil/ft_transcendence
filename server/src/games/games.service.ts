import { Injectable } from '@nestjs/common';
import { Like } from 'typeorm';
import { ResponseOBJ } from '../utils-dto/utils.dto';
import { Game } from './entities/game.entity';

@Injectable()
export class GamesService {
    //chercher un match
    async findAllGame(responseObj: ResponseOBJ, query): Promise<string> {
        if (query['index'] as number < 1) {
            query['index'] = 1;
        }

        let liste = await Game.find({
            order: {
                createdAt: "DESC",
            },
            skip: query['index'] as number === 0 ? 0 : ((10 * query['index'] as number) - 10),
            take: 10,
        })

        responseObj.data = liste;
        return JSON.stringify(responseObj);
    }

    async findOneGame(responseObj: ResponseOBJ, query): Promise<string> {
        let username = query['username'] as string
        let liste = await Game.find({
            where: [
                { username_player1: Like(`%${username}%`) },
                { username_player2: Like(`%${username}%`) }
            ],
            take: 10,
        })
        responseObj.data = liste;
        return JSON.stringify(responseObj);
    }

}
