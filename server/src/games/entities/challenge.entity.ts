
import { Entity, CreateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';


@Entity({ name: "challenges" })
export class Challenge extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: false })
    username_player1: string;

    @Column({ unique: false })
    username_player2: string;

    @Column({ unique: false })
    socketID_player1: string;

    @Column({ unique: false })
    challengeID: string;

    @Column({ unique: false })
    mapName: string;

    @CreateDateColumn({})
    createdAt: Date
}