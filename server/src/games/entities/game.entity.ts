
import { Entity, CreateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity } from 'typeorm';


@Entity({ name: "games" })
export class Game extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: false })
    username_player1: string;

    @Column({ unique: false })
    username_player2: string;

    @Column({ unique: false })
    gameID: string;

    @Column({ default: 0 })
    score_player1: number

    @Column({ default: 0 })
    score_player2: number

    @Column({ unique: false })
    mapName: string;

    @CreateDateColumn({})
    createdAt: Date
}