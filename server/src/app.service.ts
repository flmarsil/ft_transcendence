import { Injectable, CACHE_MANAGER, Inject } from '@nestjs/common';
import { Controller, Header, HttpCode, Get, Post, Delete, Param, Query, Res, Req, UseInterceptors, UploadedFile } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { UsersService } from './users/users.service'
import { comparePassword, hashPassword, initResponse, makeCode } from './utils-dto/utils.dto';
import { Cache } from 'cache-manager';
import { Repository } from 'typeorm';
import { UserSession } from './userSessions/entities/userSession.entity'
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { getIntraApiLocation } from './apiConfig'
import { response } from 'express';
import { DoubleAuth } from './userSessions/entities/doubleAuth.entity';
import { MailService } from './mail/mail.service'


@Injectable()
export class AppService {
    constructor(
        private http: HttpService,
        @InjectRepository(UserSession)
        private userSessionRepository: Repository<UserSession>,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private mailService: MailService,
    ) { }

    async getSession(login: string, cb) {
        let result = await UserSession.findOne({
            where: [
                { 'login': login },
            ]
        })

        if (result == undefined) {

            let user = await User.findOne({
                where: [
                    { 'login': login },
                ]
            })

            let crypto = require("crypto");
            let acces_token = crypto.randomBytes(30).toString('hex');
            let tempOBJ = {
                'login': login,
                'token': acces_token,
            }

            await UsersService.prototype.saveSessionWithUser(user, tempOBJ)
            await user.save()
            cb(null, tempOBJ)
        } else {

            //console.log('fetched user Session :', result)
            let cookieOBJ = {
                'login': result.login,
                'token': result.token,
            }
            this.cacheManager.set(result['login'], JSON.stringify(cookieOBJ))
            cb(null, cookieOBJ)

        }
    }

    async isConnected(@Req() req, @Res() res, @Query() query): Promise<string> {

        //console.log("------ islogin function")

        let login = undefined
        let responseObj = initResponse(query)
        let cookieOBJ

        try {
            cookieOBJ = await JSON.parse(req.cookies['ft_transcendence_cookie'] as string)
            login = cookieOBJ.login;
        } catch {
            //console.log("cookieOBJ undefined");
            responseObj.data = "false"
            res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
            res.send(JSON.stringify(responseObj))
            return
        }

        if (login != undefined) {
            let cacheOBJ;
            const cache = await this.cacheManager.get(login)
            if (cache != undefined) {

                try {
                    if (typeof (cache) === 'string') {// je ne sais pas pourquoi, mais parfois cache est de type string ...
                        cacheOBJ = JSON.parse(cache)
                    } else {
                        cacheOBJ = cache
                    }
                } catch {
                    responseObj.data = "false"
                    res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
                    res.send(JSON.stringify(responseObj))
                    return
                }

                if ((cookieOBJ['login'] === undefined) && (cookieOBJ['token'] === undefined)) {
                    responseObj.data = "false"
                    res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
                    res.send(JSON.stringify(responseObj))
                    return
                }

                if (cookieOBJ['token'] != cacheOBJ['token']) {
                    responseObj.data = "false"
                    res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
                    res.send(JSON.stringify(responseObj))
                    return
                } else {
                    responseObj.data = "true"
                    res.send(JSON.stringify(responseObj));
                    return
                }

            } else {
                const count = await DoubleAuth.count({
                    where: [
                        { 'login': login },
                    ]
                })

                if (count != 0) {
                    responseObj.data = "2fa"
                } else {
                    res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
                    responseObj.data = "false"
                }

            }

        } else {
            res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
            responseObj.data = "false"
        }
        res.send(JSON.stringify(responseObj))
    }

    async login(@Res() res, @Query() query) {
        let crypto = require("crypto");
        let responseObj = initResponse(query)

        if (query['code']) {
            this.http.request({
                url: "/oauth/token",
                method: "post",
                baseURL: process.env.API42_HOST,
                data: {
                    "grant_type": "authorization_code",
                    "scope": "public",
                    "client_id": process.env.API42_CLIENT_ID,
                    "client_secret": process.env.API42_CLIENT_SECRET,
                    "code": query['code'],
                    "redirect_uri": process.env.FT_LOGIN,
                    "state": await crypto.randomBytes(30).toString('hex')
                }
            }).subscribe((response) => {
                this.http.request({
                    url: "/v2/me",
                    method: "get",
                    baseURL: process.env.API42_HOST,
                    headers: {
                        "Authorization": "Bearer " + response.data.access_token,
                    }

                }).subscribe(async (res2) => {
                    let acces_token = await crypto.randomBytes(30).toString('hex');

                    const count = await User.count({
                        where: [
                            { 'login': res2.data['login'] as string },
                        ]
                    })

                    if (count == 0) {
                        //créer le compte
                        UsersService.prototype.new_createAccount(res2.data)
                        let cookieOBJ = {
                            'login': res2.data['login'],
                            'token': acces_token,
                        }

                        const session = new UserSession();
                        session.login = res2.data['login'];
                        session.token = acces_token
                        await session.save();

                        res.set({ 'Set-Cookie': `ft_transcendence_cookie=${JSON.stringify(cookieOBJ)} ; Path=/; SameSite=Lax;` })
                        this.cacheManager.set(res2.data['login'], JSON.stringify(cookieOBJ))

                    } else {
                        //compte existe deja
                        responseObj.data = {
                            'status': await UsersService.prototype.doubleFA_auth_check(res2.data['login']),
                        }

                        let cookieOBJ = {
                            'login': res2.data['login'],
                            'token': acces_token,
                        }

                        if (responseObj.data['status'] === "OK") {


                            ///verifier si la session existe pas deja
                            let oldsession = await UserSession.findOne({
                                where: [
                                    { 'login': res2.data['login'] },
                                ]
                            })


                            if (oldsession == undefined) {
                                const session = new UserSession();
                                session.login = res2.data['login'];
                                session.token = acces_token
                                await session.save();
                            } else {
                                cookieOBJ.token = oldsession.token
                            }

                            res.set({ 'Set-Cookie': `ft_transcendence_cookie=${JSON.stringify(cookieOBJ)} ; Path=/; SameSite=Lax;` })
                            this.cacheManager.set(res2.data['login'], JSON.stringify(cookieOBJ))


                        } else if (responseObj.data['status'] === "2FA") {
                            /// créer un 2FA dans la base de donnée

                            const tmpCode = makeCode(6)
                            const doubleFA = new DoubleAuth()
                            doubleFA.login = res2.data['login']
                            doubleFA.code = await hashPassword(tmpCode) as string
                            await doubleFA.save()


                            //envoyer le mail ici
                            const user = await User.findOne({
                                where: [
                                    { 'login': cookieOBJ['login'] },
                                ]
                            })
                            await this.mailService.sendMail(user.email, "ft_transcendence", "ft_transcendence code double authentification : " + tmpCode)

                            ///supprimer le 2fA au bout de quelque secondes
                            setTimeout(
                                function () {
                                    doubleFA.remove();
                                },
                                60000, doubleFA);

                            res.set({ 'Set-Cookie': `ft_transcendence_cookie=${JSON.stringify(cookieOBJ)} ; Path=/; SameSite=Lax;` })
                        }

                    }

                    responseObj.data = {
                        'status': "LOCATION",
                        'location': process.env.FT_HOMEPAGE
                    }

                    res.statusCode = 302
                    res.set({ 'Location': process.env.FT_HOMEPAGE });
                    res.send(JSON.stringify(responseObj))

                })
            });
        } else {// pas de code, donc renvoie sur la page login
            //console.log('pas de code')
            res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })

            responseObj.data = {
                'status': "LOCATION",
                'location': getIntraApiLocation()
            }
            res.send(JSON.stringify(responseObj))
        }


    }

    async send2faCode(@Req() req, @Query() query): Promise<string> {
        let responseObj = initResponse(query)
        let cookieOBJ

        try {
            cookieOBJ = JSON.parse(req.cookies['ft_transcendence_cookie'] as string)
        } catch {
            //console.log("cookieOBJ undefined");
            responseObj.data = {
                'status': "NO",
            }
            return JSON.stringify(responseObj)
        }


        const doubleFA = await DoubleAuth.findOne({
            where: [
                { 'login': cookieOBJ['login'] },
            ]
        })



        if (doubleFA != undefined) {
            if (comparePassword(doubleFA.code, query['authcode'] as string)) {
                responseObj.data = {
                    'status': "OK",
                }
                await doubleFA.remove()

                const session = new UserSession();
                session.login = cookieOBJ['login']
                session.token = cookieOBJ['token']

                await UserSession.createQueryBuilder()
                    .insert()
                    .into(UserSession)
                    .values(session)
                    .orIgnore()
                    .execute()
                    .catch((err) => {
                        console.log("2fa error \x1b[36m%s\x1b[0m", err);
                    });

                await this.cacheManager.set(cookieOBJ['login'], JSON.stringify(cookieOBJ))

            } else {

                let count = doubleFA.attempts

                count = count - 1
                doubleFA.attempts = count
                if (count <= 0) {
                    const tmpcode = makeCode(6)
                    doubleFA.code = await hashPassword(tmpcode) as string
                    doubleFA.attempts = 4
                    const user = await User.findOne({
                        where: [
                            { 'login': cookieOBJ['login'] },
                        ]
                    })
                    ///envoyer un mail
                    await this.mailService.sendMail(user.email, "ft_transcendence", "ft_transcendence code double authentification : " + tmpcode)

                    await doubleFA.save()

                    ///supprimer le 2fA au bout de quelque secondes
                    setTimeout(
                        function () {
                            doubleFA.remove();
                        },
                        60000, doubleFA);
                }


                responseObj.data = {
                    'status': "NO",
                    'attempt': count
                }
            }
        } else {
            responseObj.data = {
                'status': "NO",
            }
        }

        return JSON.stringify(responseObj)
    }
}