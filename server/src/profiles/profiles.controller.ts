import { Controller, Get, Post, Query, Param, } from '@nestjs/common';
import { ProfilesService } from './profiles.service';
import { initResponse } from '../utils-dto/utils.dto';

@Controller('api/v1/profiles')
export class ProfilesController {
  constructor(private readonly profilesService: ProfilesService) { }

  //profiles

  @Get('/search')
  async searchProfile(@Query() query): Promise<string> {
    return await this.profilesService.searchProfile(initResponse(query), query);
  }

  @Get('/blocks')
  async getListOfProfileBlocked(@Query() query): Promise<string> {
    return await this.profilesService.getListOfProfileBlocked(initResponse(query), query);
  }

  @Post('/block')
  async blockProfile(@Query() query): Promise<string> {
    return await this.profilesService.blockProfile(initResponse(query), query);
  }

  @Post('/unblock')
  async unblockProfile(@Query() query): Promise<string> {
    return await this.profilesService.unblockProfile(initResponse(query), query);
  }

  @Get(':username')
  async getProfileInfo(@Param('username') username: string, @Query() query): Promise<string> {
    return await this.profilesService.getProfileInfo(initResponse(query), username, query);
  }

  /*@Post('/message')
  async sendProfileMessage(@Query() query): Promise<string> {
    return await this.profilesService.sendProfileMessage(initResponse(query), query);
  }

  @Get('/messages')
  async getListOfProfileMessages(@Query() query): Promise<string> {
    return await this.profilesService.getListOfProfileMessages(initResponse(query), query);
  }*/

}
