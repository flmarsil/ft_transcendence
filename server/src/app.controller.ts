import { Controller, Get, Post, Req, HttpCode, Param, Query, Res, UseInterceptors, UploadedFile, Header, Inject, CACHE_MANAGER, Put } from '@nestjs/common';
import { AppService } from './app.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { getIntraApiLocation } from './apiConfig'
import { MailService } from './mail/mail.service'
import { initResponse } from './utils-dto/utils.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
    private mailService: MailService,
  ) { }

  /*@Get()
  //@Header('Location', '/app/index.html')
  @HttpCode(302)
  getHome(@Req() req, @Res() res, @Query() query) {
    return this.appService.getHome(req, res, query)
  }*/

  ///////////Nouveau

  @Get()
  //TODO:
  //  @Header('Location', '/index.html')
  @Header('Location', process.env.FT_HOMEPAGE)
  @HttpCode(302)
  getHome(@Req() req, @Res() res, @Query() query) {

    if (process.env.FT_AUTHENTICATION === String("false")) {
      let cookieOBJ
      try {
        cookieOBJ = JSON.parse(req.cookies['ft_transcendence_cookie'] as string)
      } catch {
        //console.log("cookieOBJ undefined");
      }

      if (query['login']) {

        let cookieOBJ = {
          'login': req.query['login'],
          'token': "mytoken",
        }

        res.set({ 'Set-Cookie': `ft_transcendence_cookie=${JSON.stringify(cookieOBJ)} ; Path=/; SameSite=Lax;` })

      } else {
        if (cookieOBJ === undefined) {
          let cookieOBJ = {
            'login': "CompteTestLogin",
            'token': "mytoken",
          }
          res.set({ 'Set-Cookie': `ft_transcendence_cookie=${JSON.stringify(cookieOBJ)} ; Path=/; SameSite=Lax;` })
        } else {
          if (cookieOBJ.hasOwnProperty('login') === true) {
            if (cookieOBJ['login'] === undefined) {
              let cookieOBJ = {
                'login': "CompteTestUsername",
                'token': "mytoken",
              }
              res.set({ 'Set-Cookie': `ft_transcendence_cookie=${JSON.stringify(cookieOBJ)} ; Path=/; SameSite=Lax;` })
            }
          }
        }
      }
    }
    res.send("ok")
  }

  @Get('/app/')
  //@HttpCode(301)
  //@Header('Location', '/app/')
  getApp(@Res() res, @Param('file') file) {
    return res.sendFile('index.html', { root: './singlePageApp' });
  }


  @Get('/app/:file(*)')
  // @HttpCode(301)
  //@Header('Location', '/app/')
  getApp2(@Res() res, @Param('file') file) {
    //console.log('--------- FILE :', file)
    return res.sendFile(file, { root: './singlePageApp' });
  }

  @Get('/api/v1/login')
  async login(@Res() res, @Query() query) {
    this.appService.login(res, query)
  }

  @Get('/api/v1/isConnected')
  async isConnected(@Req() req, @Res() res, @Query() query) {
    let cookieOBJ
    try {
      cookieOBJ = JSON.parse(req.cookies['ft_transcendence_cookie'] as string)
    } catch {
      //console.log("cookieOBJ undefined");
    }

    if (cookieOBJ === undefined) {
      let responseObj = initResponse(query)
      responseObj.data = "false"
      res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
      res.send(JSON.stringify(responseObj))
      return
    }

    if (cookieOBJ['login'] === undefined) {
      let responseObj = initResponse(query)
      responseObj.data = "false"
      res.set({ 'Set-Cookie': `ft_transcendence_cookie=? ; Path=/; SameSite=Lax;` })
      res.send(JSON.stringify(responseObj))
      return
    }

    if (process.env.FT_AUTHENTICATION === String("false")) {
      let responseObj = initResponse(query)
      responseObj.data = "true"
      res.send(JSON.stringify(responseObj))
      return
    } else {
      await this.appService.isConnected(req, res, query)
    }
  }

  @Put('/api/v1/send2faCode')
  async send2faCode(@Req() req, @Query() query) {
    //console.log("------ 2FA")
    // await this.mailService.sendMail("mail", "ft_transcendence", "ft_transcendence code double authentification : 287562")
    return await this.appService.send2faCode(req, query)
  }

  /*@Get('/app/:file')
  getApp(@Res() res, @Param('file') file) {
    if (file == 'home') {
      return res.sendFile('index.html', { root: './singlePageApp' });
    } else {
      return res.sendFile(file, { root: './singlePageApp' });
    }
  }*/

  @Get('/avatar/:file')
  getAvatar(@Res() res, @Param('file') file) {
    return res.sendFile(file, { root: './uploadstore' });
  }

}
