import { CacheModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { DirectMessagesService } from './direct-messages.service';
import { DirectMessagesController } from './direct-messages.controller';
import { AuthentificationMiddleware } from '../middlewares/authentification.middleware';
import { DirectMessageThreads } from './entities/direct-message-threads.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([DirectMessageThreads]), CacheModule.register({ ttl: 0 })],
  exports: [TypeOrmModule],
  controllers: [DirectMessagesController],
  providers: [DirectMessagesService]
})
export class DirectMessagesModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthentificationMiddleware)
      .forRoutes(DirectMessagesController);
  }
}
