import { User } from "../../users/entities/user.entity";

export class CreateDirectMessageThreadDto {
    id: number;
    lastEmitterUsername: string;
    lastEmitterLogin: string;
    lastMessageSeen: boolean;
    user1: User;
    user2: User;
}