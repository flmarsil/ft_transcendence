/*
** the directMessages entity contains the sender's username,
** the message itself and refers to a discution entity
*/

import { Entity, CreateDateColumn, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, } from 'typeorm';
import { DirectMessageThreads } from './direct-message-threads.entity';

@Entity({ name: "direct_messages" })
export class DirectMessages extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne((type) => DirectMessageThreads, (directMessageThread) => directMessageThread.id,
        {
            cascade: true,
            onDelete: 'CASCADE', //when deleting a directMessageThread, the rows related in this table are deleted.
        })
    directMessageThread: DirectMessageThreads;

    //to see the message time and date...
    @CreateDateColumn()
    createdAt: Date

    @Column()
    emitterUsername: string;

    //TODO: this must be handled.
    //note: cannot send more thatn 150 characters at a time...
    @Column({ type: "varchar", length: 255, })
    message: string;
}