import { Injectable, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { addErrorToResponseObj, addInternalServerErrorToResponseObj, ResponseOBJ, ResponseStatus } from '../utils-dto/utils.dto';
import { Brackets, Repository } from 'typeorm';
import { DirectMessageThreads } from './entities/direct-message-threads.entity';
import { User } from '../users/entities/user.entity'
import { DirectMessages } from './entities/direct-messages.entity';
import { UsersService } from '../users/users.service';
import { CreateDirectMessageDto } from './dto/create-direct-message.dto';
import { UpdateDirectMessageThreadDto } from './dto/update-direct-message-thread.dto';
import { Dir } from 'fs';
import { last } from 'rxjs';
import { json, response } from 'express';
import { Friend, FriendState } from '../friends/entities/friend.entity';

@Injectable()
export class DirectMessagesService {
    constructor(
        @InjectRepository(DirectMessageThreads)
        private DirectMessageThreadRepository: Repository<DirectMessageThreads>,
    ) { }
    /* **************************************************************************************** */
    /* PRIVATE HELPER FUNCTIONS SECTION                                                         */
    /* **************************************************************************************** */

    private async getUserFromItsUsername(username: string): Promise<User> {
        return await User.findOne({ where: { username: username } });
    }

    private async getDirectMessageThreadFromThreadId(id: Number,): Promise<DirectMessageThreads> {
        return await DirectMessageThreads.createQueryBuilder("directMessageThread")
            .leftJoinAndSelect("directMessageThread.user1", "user1")
            .leftJoinAndSelect("directMessageThread.user2", "user2")
            .where("directMessageThread.id = :idThread", { idThread: id })
            .getOne();
    }

    private async getDirectMessageThreadFromMyLoginAndOtherUsername(loginMe: String, usernameOther: String): Promise<DirectMessageThreads> {
        return await DirectMessageThreads.createQueryBuilder("directMessageThread")
            .leftJoinAndSelect("directMessageThread.user1", "user1")
            .leftJoinAndSelect("directMessageThread.user2", "user2")
            .where(new Brackets(qb1 => {
                qb1.where("user1.login = :login1", { login1: loginMe })
                    .andWhere("user2.username = :username1", { username1: usernameOther })
            }))
            .orWhere(new Brackets(qb2 => {
                qb2.where("user2.login = :login2", { login2: loginMe })
                    .andWhere("user1.username = :username2", { username2: usernameOther })
            }))
            .getOne();
    }

    private async getDirectMessageThreadFromUsers(user1: User, user2: User): Promise<DirectMessageThreads> {
        return await DirectMessageThreads.findOne({
            where: [
                { user1: user1, user2: user2 }, //user1 == user1 AND user2 == user2
                //OR
                { user1: user2, user2: user1 }, //user1 == user2 AND ...
            ]
        });
    }

    private initDirectMessageThreadBewteenTwoUsers(userSource: User, userTarget: User): DirectMessageThreads {
        const newDirectMessageThread: DirectMessageThreads = new DirectMessageThreads();
        newDirectMessageThread.user1 = userSource;
        newDirectMessageThread.lastEmitterUsername = userSource.username;
        newDirectMessageThread.lastEmitterLogin = userSource.login;
        newDirectMessageThread.user2 = userTarget;
        //console.log(`New directMessageThread created between ${userSource.username} and ${userTarget.username}.`)
        return newDirectMessageThread;
    }

    private initDirectMessageFromEmitter(usernameEmitter: string, message: string, directMessageThread: DirectMessageThreads): DirectMessages {
        const newDirectMessage: DirectMessages = new DirectMessages();
        newDirectMessage.message = message;
        newDirectMessage.emitterUsername = usernameEmitter;
        newDirectMessage.directMessageThread = directMessageThread;
        return newDirectMessage;
    }

    //to find out if the userTarget blocked the userSource...
    private async userTargetBlockedUserSource(userTarget: User, userSource: User): Promise<Boolean> {
        let user_first_login: string;
        let user_second_login: string;
        let state_request_to_avoid: FriendState;

        if (userTarget.login < userSource.login) {
            user_first_login = userTarget.login; //target is first
            user_second_login = userSource.login;
            state_request_to_avoid = FriendState.BLOCK_BY_FIRST;
        }
        else {
            user_first_login = userSource.login;
            user_second_login = userTarget.login; //target is second
            state_request_to_avoid = FriendState.BLOCK_BY_SECOND;
        }
        const blocked_frienship: Friend = await Friend.findOne({
            where: [
                {
                    'user_first_login': user_first_login,
                    'user_second_login': user_second_login,
                    'state': state_request_to_avoid
                },
            ]
        })
        //TODO remove me....
        //console.log("blocked friendship: ", blocked_frienship);
        if (blocked_frienship)
            return true;
        return false;
    }
    /* **************************************************************************************** */
    /* SERVICE FUNCTIONS SECTION                                                                */
    /* **************************************************************************************** */

    //TODO remove TMP FUNC FOR DEV
    async directMessageThreadCreateOne(responseObj: ResponseOBJ, query): Promise<string> {
        try {
            const userSource = await this.getUserFromItsUsername(query['usernameSource']);
            const userTarget = await this.getUserFromItsUsername(query['usernameTarget']);
            if (!userSource || !userTarget) {
                addErrorToResponseObj(responseObj, "usernameTarget or usernameSource doesnt exist.", 0);
                return JSON.stringify(responseObj)
            }
            if (userSource.username == userTarget.username) {
                addErrorToResponseObj(responseObj, "usernameTarget and usernameSource cannot be equal", 1);
                return JSON.stringify(responseObj)
            }
            let directMessageThread: DirectMessageThreads = await this.getDirectMessageThreadFromUsers(userSource, userTarget);
            if (directMessageThread) {
                addErrorToResponseObj(responseObj, `directMessageThread already exists between ${userTarget.username} && ${userSource.username}.`, 2);
                return JSON.stringify(responseObj)
            }
            directMessageThread = this.initDirectMessageThreadBewteenTwoUsers(userSource, userTarget);
            await DirectMessageThreads.save(directMessageThread);
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    //TODO this func should be called by backend in a websocket event...
    //and not from an exposed route.
    async directMessageCreateOne(responseObj: ResponseOBJ, directMessageDto: CreateDirectMessageDto): Promise<string> {
        try {
            if (directMessageDto.usernameSource === directMessageDto.usernameTarget) {
                addErrorToResponseObj(responseObj, "usernameTarget and usernameSource cannot be equal", 0);
                return JSON.stringify(responseObj)
            }
            const userSource = await this.getUserFromItsUsername(directMessageDto['usernameSource']);
            const userTarget = await this.getUserFromItsUsername(directMessageDto['usernameTarget']);
            if (!userSource || !userTarget) {
                addErrorToResponseObj(responseObj, "usernameTarget or usernameSource doesnt exist.", 1);
                return JSON.stringify(responseObj)
            }

            //abort if target has blocked source.
            if (await this.userTargetBlockedUserSource(userTarget, userSource) === true) {
                addErrorToResponseObj(responseObj, `userTarget ${userTarget.username} blocked userSource ${userSource.username}.`, 2);
                return JSON.stringify(responseObj)
            }

            let directMessageThread: DirectMessageThreads = await this.getDirectMessageThreadFromUsers(userSource, userTarget);
            if (!directMessageThread) { //create a new directMessageThread for the two users.
                directMessageThread = this.initDirectMessageThreadBewteenTwoUsers(userSource, userTarget);
            }
            else {
                directMessageThread.lastMessageSeen = false;
                directMessageThread.lastEmitterLogin = userSource.login;
                directMessageThread.lastEmitterUsername = userSource.username;
                directMessageThread.updatedAt = new Date();
            }
            const newDirectMessage = this.initDirectMessageFromEmitter(userSource.username, directMessageDto['message'], directMessageThread);
            await DirectMessages.save(newDirectMessage);
            //console.log(`new directMessage from ${userSource.username} to ${userTarget.username}, added in db.`);
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    async directMessageThreadUpdateToSeen(responseObj: ResponseOBJ, @Query() query, updateDirectMessageThreadDto: UpdateDirectMessageThreadDto): Promise<string> {
        try {
            const directMessageThread = await this.getDirectMessageThreadFromThreadId(updateDirectMessageThreadDto.id);
            if (!directMessageThread) {
                addErrorToResponseObj(responseObj, `directMessageThread ${updateDirectMessageThreadDto.id} doesnt exist (anymore).`, 0);
                return JSON.stringify(responseObj);
            }
            if (directMessageThread.lastMessageSeen === true) //do nothing...
                return JSON.stringify(responseObj);
            if (directMessageThread.lastEmitterLogin === query['login']) {
                addErrorToResponseObj(responseObj, `${query['login']} has sent the last message. Cannot update bool to seen.`, 1);
                return JSON.stringify(responseObj);
            }
            if (directMessageThread.user1.login !== query['login'] && directMessageThread.user2.login !== query['login']) {
                addErrorToResponseObj(responseObj, `${query['login']} does not belong to this directMessageThread (${directMessageThread.id})`, 2);
                return JSON.stringify(responseObj);
            }
            directMessageThread.lastMessageSeen = true;
            await DirectMessageThreads.save(directMessageThread);
            //console.log(`directMessageThread ${directMessageThread.id} has been (re)updated to seen for ${query.login}`)
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    async getListOfDirectMessageThreads(responseObj: ResponseOBJ, @Query() query): Promise<string> {
        try {
            const listDirectMessageThreads: DirectMessageThreads[] = await DirectMessageThreads.createQueryBuilder("directMessageThread")
                .leftJoinAndSelect("directMessageThread.user1", "user1")
                .leftJoinAndSelect("directMessageThread.user2", "user2")
                .where("user1.login = :user1Login", { user1Login: query['login'] })
                .orWhere("user2.login = :user2Login", { user2Login: query['login'] })
                .orderBy("directMessageThread.updatedAt", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mlist of directMessageThreads for user ${query['login']}, since all time:\x1b[0m\n`, listDirectMessageThreads);
            responseObj.data = listDirectMessageThreads;
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    async getListOfDirectMessagesInThread(responseObj: ResponseOBJ, @Query() query): Promise<string> {
        try {
            //check that the login is in the private thread...
            const directMessageThread: DirectMessageThreads = await this.getDirectMessageThreadFromThreadId(query['threadId']);
            if (!directMessageThread) {
                addErrorToResponseObj(responseObj, `directMessageThread ${query['threadId']} doesnt exist (anymore).`, 2);
                return JSON.stringify(responseObj);
            }
            if (directMessageThread.user1.login !== query['login'] && directMessageThread.user2.login !== query['login']) {
                addErrorToResponseObj(responseObj, `login ${query['login']} doesnt belong in directMessageThread ${query['threadId']}`, 3);
                return JSON.stringify(responseObj);
            }
            const listDirectMessagesInThread: DirectMessages[] = await DirectMessages.createQueryBuilder("directMessage")
                .leftJoin("directMessage.directMessageThread", "thread")
                .where("thread.id = :threadId", { threadId: query['threadId'] })
                .orderBy("directMessage.createdAt", 'ASC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mlist of directMessages in thread ${query['threadId']}, for user ${query['login']}, since all time:\x1b[0m\n`, listDirectMessagesInThread);
            responseObj.data = listDirectMessagesInThread;
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    async getDirectMessagesThreadExists(responseObj: ResponseOBJ, @Query() query): Promise<string> {
        try {
            //check that the login and the username do have an existing thread in common
            const directMessageThread: DirectMessageThreads = await this.getDirectMessageThreadFromMyLoginAndOtherUsername(query['login'], query['username'])
            if (!directMessageThread) {
                addErrorToResponseObj(responseObj, `there is no existing directMessage thread between login ${query['login']} and username ${query['username']}`, 2);
                return JSON.stringify(responseObj);
            }
            responseObj.data = directMessageThread;
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }
}
