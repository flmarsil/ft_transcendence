import { Test, TestingModule } from '@nestjs/testing';
import { DirectMessagesController } from './direct-messages.controller';
import { DirectMessagesService } from './direct-messages.service';

describe('DirectMessagesController', () => {
  let controller: DirectMessagesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DirectMessagesController],
      providers: [DirectMessagesService],
    }).compile();

    controller = module.get<DirectMessagesController>(DirectMessagesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
