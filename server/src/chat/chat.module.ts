import { CacheModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatabaseModule } from '../database/database.module';
import { ChatController } from './chat.controller';

@Module({
    imports: [TypeOrmModule.forFeature([]), CacheModule.register({ ttl: 0 }), DatabaseModule, ChatModule],
    providers: [],
    controllers: [ChatController]
})
export class ChatModule { }