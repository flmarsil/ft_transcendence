import { Controller, Header, Get, Res, Param, } from '@nestjs/common';

@Controller('api/v1')
export class ChatController {
    constructor() { }

    @Get('/chat')
    @Header('Content-Type', 'text/html; charset=utf-8')
    getChat(@Res() res) {
        //console.log(`index.html sent.`);
        return res.sendFile('index.html', { root: './src/chat/' });
    }

    @Get('/chat/:file')
    @Header('Content-Type', 'text/html; charset=utf-8')
    getChatJsFile(@Res() res, @Param('file') file) {
        //console.log(`${file} sent.`);
        return res.sendFile(file, { root: './src/chat/' });
    }
}