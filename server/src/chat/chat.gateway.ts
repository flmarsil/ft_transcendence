import { CACHE_MANAGER, Inject, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { OnGatewayInit, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer, WsResponse, OnGatewayConnection, OnGatewayDisconnect } from "@nestjs/websockets";
import { Server, Socket } from 'socket.io';
import { UserSession } from "../userSessions/entities/userSession.entity";
import { Repository } from "typeorm";
import { Cache } from 'cache-manager';
import { User } from "../users/entities/user.entity";
import { Friend, FriendState } from "../friends/entities/friend.entity";
import { DirectMessagesService } from "../direct-messages/direct-messages.service"
import { initResponse } from "../utils-dto/utils.dto";
import { CreateDirectMessageDto } from "../direct-messages/dto/create-direct-message.dto";
import { ChatChannel } from "../chatChannels/entities/chatChannel.entity";
import { ChatChannelsService } from "../chatChannels/chatChannels.service";
import { ChatChannelBannedOrMutedUser, ChatChannelUserState } from "../chatChannels/entities/chatChannelBannedOrMutedUser.entity";

@WebSocketGateway({
    cors: {
        origin: ["http://localhost:3454", "http://localhost:8080"],
        methods: ["GET", "POST", "PUT", "DELETE"],
        allowedHeaders: [
            "X-Requested-With",
            "X-HTTP-Method-Override",
            "Content-Type",
            "Accept",
            "Observe",
            "Content-Length",
            "Accept-Encoding",
            "X-CSRF-Token",
            "Authorization",
        ],
        credentials: true,
    },
})//{ transports: ['websocket'] })
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

    constructor(
        @InjectRepository(UserSession)
        private userSessionRepository: Repository<[UserSession, User]>,
        @Inject(CACHE_MANAGER) private cacheManager: Cache
    ) { }


    async isConnected(
        socket: Socket,
        socketId: string,
        str: string
    ): Promise<number> {
        var cookie = require("cookie");
        var cookies = cookie.parse(str);

        let data;
        try {
            data = await JSON.parse(cookies["ft_transcendence_cookie"]);
        } catch (error) {
            //console.log("Json error :", error);
            return -1;
        }

        if (data.login === undefined || data.token === undefined) {
            return -2;
        }

        const cachelogin = await this.cacheManager.get(socketId);

        const user = await User.findOne({
            where: { login: data.login },
        });

        if (user === undefined) {
            return -5;
        }

        if (process.env.FT_AUTHENTICATION === String("false")) {
            if (data.hasOwnProperty("login") === true) {
                this.cacheManager.set(socketId, data.login);
                socket.join(user.username);

                await User.createQueryBuilder()
                    .update()
                    .set({ status: 1 })
                    .where("login=:login ", { login: data.login as string })
                    .execute()
                    .catch((err) => {
                        //console.log("\x1b[36m%s\x1b[0m", err);
                    });

                return 1;
            }
            return -20;
        }

        if (cachelogin === data.login) {
            //console.log("user authenticated :", data.login);
            socket.join(user.username);
            return 1;
        }

        let result = await UserSession.findOne({
            where: [{ login: data.login, token: data.token }],
        });

        if (result == undefined) {
            return -4;
        }

        this.cacheManager.set(socketId, result.login);

        await User.createQueryBuilder()
            .update()
            .set({ status: 1 })
            .where("login=:login ", { login: result.login as string })
            .execute()
            .catch((err) => {
                //console.log("\x1b[36m%s\x1b[0m", err);
            });

        //console.log(`\x1b[32mUser connected:    \x1b[0m${result.login}`);
        socket.join(user.username);

        await User.createQueryBuilder()
            .update()
            .set({ status: 1 })
            .where("login=:login ", { login: result.login as string })
            .execute()
            .catch((err) => {
                //console.log("\x1b[36m%s\x1b[0m", err);
            });

        return 1;
    }

    private logger = new Logger('ChatGateway');

    async afterInit(server: Server) {
        this.logger.log("Initialised");
        server.use(async (socket, next) => {
            let ret = 0
            if ((ret = await this.isConnected(socket, socket.id, socket.handshake.headers.cookie)) != 1) {
                let err
                //console.log("Error code :", ret)
                if (ret == -1 || ret == -2) {
                    err = new Error("{ error: 'Bad Request' }");
                    next(err);
                } else if (ret == -3 || ret == -4) {
                    err = new Error("{ error: 'Unauthorized, user not connected' }");
                    next(err);
                }
            }
            next();
        });
    }

    async handleConnection(client: Socket) {
        //console.log(`\x1b[32mclient connected to chatGateway:    \x1b[0m${client.id}`);
        client.use(async (socket, next) => {
            let ret = 0
            if ((ret = await this.isConnected(client, client.id, client.handshake.headers.cookie)) != 1) {
                let err
                if (ret == -1 || ret == -2) {
                    err = new Error("{ error: 'Bad Request' }");
                    next(err);
                } else if (ret == -3 || ret == -4) {
                    err = new Error("{ error: 'Unauthorized, user not connected' }");
                    next(err);
                }

            }
            next();
        });
    }

    async handleDisconnect(client: Socket) {
        //console.log(`\x1b[31mclient disconnected from chat: \x1b[0m${client.id}`);
        const cachelogin = await this.cacheManager.get(client.id)

        if (cachelogin != undefined) {
            await this.cacheManager.del(client.id)
            await User.createQueryBuilder()
                .update()
                .set({ status: 0 })
                .where('login=:login ', { 'login': cachelogin as string })
                .execute()
                .catch((err) => {
                    //console.log('\x1b[36m%s\x1b[0m', err);
                });
        }
    }

    @WebSocketServer()
    server: Server;


    @SubscribeMessage("directMessage")
    async handleDirectMessageRequest(client: Socket, data: any) {
        //1 :check si friend
        //2 :save message dans database
        //3 :envoyer message au sender et au receiver

        //console.log("------- message :", data)

        const cachelogin = await this.cacheManager.get(client.id)

        //1
        const User_sender = await User.findOne({
            where: [
                { 'login': cachelogin },
            ],
        })

        const User_receiver = await User.findOne({
            where: [
                { 'username': data.username_receiver },
            ],
        })

        let user_first_login = ''
        let user_second_login = ''
        let positionLogin = 1

        if (User_sender.login < User_receiver.login) {
            user_first_login = User_sender.login
            user_second_login = User_receiver.login
            positionLogin = 1
        } else {
            user_first_login = User_receiver.login
            user_second_login = User_sender.login
            positionLogin = 2
        }

        const friend = await Friend.findOne({
            where: [
                {
                    'user_first_login': user_first_login,
                    'user_second_login': user_second_login,
                },
            ]
        })

        if (friend === undefined) {

            let ServeurMessage = new CreateDirectMessageDto()
            ServeurMessage.usernameSource = "Serveur"
            ServeurMessage.usernameTarget = User_sender.username
            ServeurMessage.message = "you cannot send a message to this user, please add it as a friend"
            this.server.to(User_sender.username).emit("directMessage", { 'data': ServeurMessage });
            //pas friend ou bloqué, ne rien faire ou envoyer un message d'erreur
            return
        }

        if (positionLogin === 1) {
            if (friend.user_first_login === User_sender.login && friend.state === FriendState.BLOCK_BY_FIRST) {

                let ServeurMessage = new CreateDirectMessageDto()
                ServeurMessage.usernameSource = "Serveur"
                ServeurMessage.usernameTarget = User_sender.username
                ServeurMessage.message = "you cannot send a message to this user, you have blocked this user."
                this.server.to(User_sender.username).emit("directMessage", { 'data': ServeurMessage });
                //pas friend ou bloqué, ne rien faire ou envoyer un message d'erreur
                return
            } else if (friend.user_first_login === User_sender.login && friend.state === FriendState.BLOCK_BY_SECOND) {

                let ServeurMessage = new CreateDirectMessageDto()
                ServeurMessage.usernameSource = "Serveur"
                ServeurMessage.usernameTarget = User_sender.username
                ServeurMessage.message = "you cannot send a message to this user, this user has blocked you."
                this.server.to(User_sender.username).emit("directMessage", { 'data': ServeurMessage });
                //pas friend ou bloqué, ne rien faire ou envoyer un message d'erreur
                return
            }
        } else if (positionLogin === 2) {
            if (friend.user_second_login === User_sender.login && friend.state === FriendState.BLOCK_BY_FIRST) {

                let ServeurMessage = new CreateDirectMessageDto()
                ServeurMessage.usernameSource = "Serveur"
                ServeurMessage.usernameTarget = User_sender.username
                ServeurMessage.message = "you cannot send a message to this user, this user has blocked you."
                this.server.to(User_sender.username).emit("directMessage", { 'data': ServeurMessage });
                //pas friend ou bloqué, ne rien faire ou envoyer un message d'erreur
                return
            } else if (friend.user_second_login === User_sender.login && friend.state === FriendState.BLOCK_BY_SECOND) {

                let ServeurMessage = new CreateDirectMessageDto()
                ServeurMessage.usernameSource = "Serveur"
                ServeurMessage.usernameTarget = User_sender.username
                ServeurMessage.message = "you cannot send a message to this user, you have blocked this user."
                this.server.to(User_sender.username).emit("directMessage", { 'data': ServeurMessage });
                //pas friend ou bloqué, ne rien faire ou envoyer un message d'erreur
                return
            }

        }

        //2 
        //sauvegarder le message dans la base de données

        if (friend.state !== FriendState.FRIEND) {
            let ServeurMessage = new CreateDirectMessageDto()
            ServeurMessage.usernameSource = "Serveur"
            ServeurMessage.usernameTarget = User_sender.username
            ServeurMessage.message = "you cannot send a message to this user, please add it as a friend"
            this.server.to(User_sender.username).emit("directMessage", { 'data': ServeurMessage });
            //pas friend ou bloqué, ne rien faire ou envoyer un message d'erreur
            return
        }

        const directmessage = new CreateDirectMessageDto()
        directmessage.usernameSource = User_sender.username
        directmessage.usernameTarget = data.username_receiver
        directmessage.message = data.message
        await DirectMessagesService.prototype.directMessageCreateOne(initResponse({}), directmessage)


        //3
        this.server.to(User_sender.username).emit("directMessage", { 'data': directmessage });
        this.server.to(User_receiver.username).emit("directMessage", { 'data': directmessage });
    }


    @SubscribeMessage("joinRoom")
    async handleJoinRoomMessageRequest(client: Socket, data: any) {

        const cachelogin = await this.cacheManager.get(client.id)

        const user = await User.findOne({
            where: [
                { 'login': cachelogin },
            ],
        })


        ///verif si il a le droit de join une room ici:

        client.join(data.toString())


        let ServeurMessage = {
            usernameSource: "Serveur",
            roomID: data,
            message: user.username + " joined this live chat."

        }

        this.server.to(data.toString()).emit("liveChatMessage", {
            'data': ServeurMessage
        });


    }

    @SubscribeMessage("messageToRoom")
    async handRoomMessageRequest(client: Socket, data: any) {

        const cachelogin = await this.cacheManager.get(client.id) as string;

        const user = await User.findOne({
            where: [
                { 'login': cachelogin },
            ],
        })

        const alreadyBannedUser: ChatChannelBannedOrMutedUser = await ChatChannelsService.prototype.chatChannelGetBannedOrMutedUserByLoginInChatChannelById(data.roomID, cachelogin, ChatChannelUserState.MUTED);
        if (alreadyBannedUser) {
            let banExpiryDate: Date = ChatChannelsService.prototype.chatChannelGetBanExpiryDate(alreadyBannedUser);
            if (banExpiryDate) { //statement targets other than undefined variable.
                data.message = "Sorry, you are muted until: " + alreadyBannedUser.expiryDate;
                data.usernameSource = "Serveur"
                client.emit("liveChatMessage", {
                    data
                })
                return;
            }
            ChatChannelBannedOrMutedUser.remove(alreadyBannedUser);
        }

        this.server.to(data.roomID.toString()).emit("liveChatMessage", {
            'data': data
        });
    }

    @SubscribeMessage("leaveRoom")
    async handLeaveRoomMessageRequest(client: Socket, data: any) {

        const cachelogin = await this.cacheManager.get(client.id)

        const user = await User.findOne({
            where: [
                { 'login': cachelogin },
            ],
        })

        client.leave(data.toString())

        let ServeurMessage = {
            usernameSource: "Serveur",
            roomID: data,
            message: user.username + " leaves this live chat."

        }

        this.server.to(data.toString()).emit("liveChatMessage", {
            'data': ServeurMessage
        });

    }

    //data should contain: room ID (id), and username (username) of banned user
    @SubscribeMessage("bannedUser")
    async bannedUserTransmitToOthers(client: Socket, data: any) {

        //console.log("bannedUser socker event: \n", data);
        //login of requester.
        const cachelogin = await this.cacheManager.get(client.id)

        const user = await User.findOne({
            where: [
                { 'login': cachelogin },
            ],
        })

        ///verif si il a le droit d'envoyer un message ici:

        let ServeurMessage = {
            usernameSource: "Serveur",
            roomID: data.id,
            message: user.username + " has banned " + data.username + " from this live chat.",
            username: data.username,
        }


        const User_player2 = await User.findOne({
            where: { username: data.username },
        });

        if (
            User_player2.playingCurrently === true ||
            User_player2.status === 0 ||
            User_player2.status === 2
        ) {
            this.server.to(data.id.toString()).emit("liveChatBannedUser", {
                'data': ServeurMessage
            });
        } else {

            this.server.to(data.username).emit("challengeError", {
                data: "you have been banned by " + user.username + " in liveChat " + data.id,
            });

            this.server.to(data.id.toString()).emit("liveChatBannedUser", {
                'data': ServeurMessage
            });

        }
    }

    //data is the room ID (id)
    @SubscribeMessage("TryTodeleteLiveChat")
    async deletedLiveChatByOwnerTransmitToOthers(client: Socket, data) {

        //console.log("deletedLiveChat socket event: \n", data);
        //login of requester.

        const cachelogin = await this.cacheManager.get(client.id) as string;

        const user = await User.findOne({
            where: [
                { 'login': cachelogin },
            ],
        })

        /*
        deleteOneChatChannel(props.thread.id.toString()).then((response) => {
            if (response.status === BasicResponseStatus.ERROR) {
                //console.log(response?.error?.errorDescription);
                return;
            }
            //context.emit("deletedLiveChat", props.thread.id);

            //to send a socket event to say thread is deleted by owner.
        });
        */

        // 1) get list of username
        // 2) delete en back end le chatchannel
        // 3) envoyer a list socket event.

        // 1)
        const listAllUsers = await User.createQueryBuilder('user')
            .leftJoinAndSelect(ChatChannel, "chatChannelOwner", "chatChannelOwner.owner = user.id")
            .leftJoin("user.chatChannelsAsAdmin", "chatChannelsAsAdmin")
            .leftJoin("user.chatChannelsAsGuest", "chatChannelsAsGuest")
            .where('chatChannelOwner.id = :idChannel1', { idChannel1: data })
            .orWhere('chatChannelsAsAdmin.id = :idChannel2', { idChannel2: data })
            .orWhere('chatChannelsAsGuest.id = :idChannel3', { idChannel3: data })
            .getMany();

        //2)
        const chatChannel = await ChatChannelsService.prototype.getChatChannelFromItsIdAndItsOwnerLogin(Number(data), cachelogin);
        if (!chatChannel) {
            return
        }
        await ChatChannel.remove(chatChannel);

        //3)
        let ServeurMessage = {
            roomID: data,
            message: user.username + " has deleted the live chat " + data,
        }

        listAllUsers.forEach(element => {
            this.server.to(element.username).emit("liveChatDeletedByOwner", {
                'data': ServeurMessage
            });
        });

    }
}