import { Injectable } from '@nestjs/common';

@Injectable()
export class ApiService {
    //obtenir la version de l'api
    getVersion(responseObj: {}): string {
        responseObj['version'] = 1.0;
        return JSON.stringify(responseObj);
    }

    //obtenir les Conditions générales d'utilisation de l'api
    getCgu(responseObj: {}): string {
        responseObj['cgu'] = 'play hard buddy!!';
        return JSON.stringify(responseObj);
    }
}
