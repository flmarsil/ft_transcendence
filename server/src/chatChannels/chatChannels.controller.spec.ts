import { Test, TestingModule } from '@nestjs/testing';
import { ChatChannelsController } from './chatChannels.controller';
import { ChatChannelsService } from './chatChannels.service';

describe('ChatChannelsController', () => {
  let controller: ChatChannelsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChatChannelsController],
      providers: [ChatChannelsService],
    }).compile();

    controller = module.get<ChatChannelsController>(ChatChannelsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
