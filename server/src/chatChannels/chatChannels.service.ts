import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Brackets } from 'typeorm';
import { ChatChannel } from './entities/chatChannel.entity'
import { User } from '../users/entities/user.entity'
import { addErrorToResponseObj, addInternalServerErrorToResponseObj, comparePassword, hashPassword, ResponseOBJ, ResponseStatus, } from '../utils-dto/utils.dto';
import { ChatChannelBannedOrMutedUser, ChatChannelUserState } from './entities/chatChannelBannedOrMutedUser.entity';

@Injectable()
export class ChatChannelsService {

  constructor(
    @InjectRepository(ChatChannel)
    private readonly chatChannelRepository: Repository<ChatChannel>,
  ) { }

  /* **************************************************************************************** */
  /* PRIVATE HELPER FUNCTIONS SECTION                                                         */
  /* **************************************************************************************** */

  //this function can throw if the database access goes wrong
  //returns Promise<void> or Promise<User>
  private async getUserFromItsLogin(login: string): Promise<User> {
    return await User.findOne({ where: { login: login } });
  }

  //this function can throw if the database access goes wrong
  //returns Promise<void> or Promise<ChatChannel>
  //the owner is also joined in the result.
  private async getChatChannelFromItsIdWithPassword(channelId: number): Promise<ChatChannel> {
    //TODO must return the password as well.
    return await ChatChannel.createQueryBuilder("chatChannel")
      .addSelect("chatChannel.password")
      .leftJoinAndSelect("chatChannel.owner", "owner")
      .where("chatChannel.id = :id", { id: channelId })
      .getOne();
  }

  //this function can throw if the database access goes wrong
  //returns Promise<void> or Promise<ChatChannel>
  async getChatChannelFromItsIdAndItsOwnerLogin(channelId: number, potentialOwnerLogin: string): Promise<ChatChannel> {
    return await ChatChannel.createQueryBuilder("chatChannel")
      .leftJoinAndSelect("chatChannel.owner", "owner")
      .where("chatChannel.id = :id", { id: channelId })
      .andWhere("owner.login = :login", { login: potentialOwnerLogin })
      .getOne();
  }

  //returns a user if it belongs to the given chatChannel.
  //NOTE: the login can be any admin (including the owner), or any guest user.
  private async chatChannelGetAnyUserByLoginInChatChannelById(idChannel: number, login: string): Promise<User> {
    return await User.createQueryBuilder('user')
      .leftJoin("user.chatChannelsAsAdmin", "chatChannelsAsAdmin")
      .leftJoin("user.chatChannelsAsGuest", "chatChannelsAsGuest")
      .where('user.login = :loginUser', { loginUser: login })
      .andWhere(new Brackets(qb => {
        qb.where('chatChannelsAsAdmin.id = :idChannel', { idChannel: idChannel })
          .orWhere('chatChannelsAsGuest.id = :idChannel', { idChannel: idChannel })
      }))
      .getOne();
  }

  //returns an admin if it belongs to the given chatChannel.
  //NOTE: the login can be any admin (including the owner).
  private async chatChannelGetAdminUserByLoginInChatChannelById(idChannel: number, login: string): Promise<User> {
    return await User.createQueryBuilder('user')
      .leftJoin("user.chatChannelsAsAdmin", "chatChannelsAsAdmin")
      .where('user.login = :loginUser', { loginUser: login })
      .andWhere('chatChannelsAsAdmin.id = :idChannel', { idChannel: idChannel })
      .getOne();
  }

  //returns a Guest if it belongs to the given chatChannel.
  //NOTE: the login can be any admin (including the owner), or any guest user.
  private async chatChannelGetGuestUserByLoginInChatChannelById(idChannel: number, login: string): Promise<User> {
    return await User.createQueryBuilder('user')
      .leftJoin("user.chatChannelsAsGuest", "chatChannelsAsGuest")
      .where('user.login = :loginUser', { loginUser: login })
      .andWhere('chatChannelsAsGuest.id = :idChannel', { idChannel: idChannel })
      .getOne();
  }

  //returns a chatChannelBannedOrMutedUser (not an user!) if it is either banned/muted in the given chatChannel.
  async chatChannelGetEitherBannedOrMutedUserByLoginInChatChannelById(idChannel: number, login: string): Promise<ChatChannelBannedOrMutedUser> {
    return await ChatChannelBannedOrMutedUser.createQueryBuilder('chatChannelBannedOrMutedUser')
      .leftJoin("chatChannelBannedOrMutedUser.chatChannel", "chatChannel")
      .leftJoin("chatChannelBannedOrMutedUser.user", "user")
      .where('user.login = :loginUser', { loginUser: login })
      .andWhere('chatChannel.id = :idChannel', { idChannel: idChannel })
      .getOne();
  }

  //returns a chatChannelBannedOrMutedUser (not an user!) if it is banned OR muted in the given chatChannel.
  async chatChannelGetBannedOrMutedUserByLoginInChatChannelById(idChannel: number, login: string, state: ChatChannelUserState): Promise<ChatChannelBannedOrMutedUser> {
    return await ChatChannelBannedOrMutedUser.createQueryBuilder('chatChannelBannedOrMutedUser')
      .leftJoin("chatChannelBannedOrMutedUser.chatChannel", "chatChannel")
      .where('chatChannelBannedOrMutedUser.userLogin = :loginUser', { loginUser: login })
      .andWhere('chatChannel.id = :idChannel', { idChannel: idChannel })
      .andWhere('chatChannelBannedOrMutedUser.state = :state', { state: state })
      .getOne();
  }

  //return true if the User is already enrolled within the given chatChannel.
  private async chatChannelUserAlreadyBelongsToChannel(chatChannel: ChatChannel, user: User): Promise<boolean> {
    const userPresent = await User.createQueryBuilder('user')
      .leftJoinAndSelect(ChatChannel, "chatChannelOwner", "chatChannelOwner.owner = user.id")
      .leftJoin("user.chatChannelsAsAdmin", "chatChannelsAsAdmin")
      .leftJoin("user.chatChannelsAsGuest", "chatChannelsAsGuest")
      .where('user.id = :idUser', { idUser: user.id })
      .andWhere(new Brackets(qb => {
        qb.where('chatChannelOwner.id = :idChannel', { idChannel: chatChannel.id })
          .orWhere('chatChannelsAsAdmin.id = :idChannel', { idChannel: chatChannel.id })
          .orWhere('chatChannelsAsGuest.id = :idChannel', { idChannel: chatChannel.id })
      }))
      .getOne();
    if (userPresent)
      return true;
    return false;
  }

  //OK
  //just remove an user (type User) from the chatChannel's admins list, or guest list.
  private async chatChannelRemoveUser(chatChannel: ChatChannel, userToBanOrMute: User): Promise<void> {
    await ChatChannel.createQueryBuilder() //try to remvove in guest table.
      .relation(ChatChannel, "guests")
      .of(chatChannel)
      .remove(userToBanOrMute);
    await ChatChannel.createQueryBuilder()  //try to remove in admin table.
      .relation(ChatChannel, "admins")
      .of(chatChannel)
      .remove(userToBanOrMute);
  }


  //returns undefined if the expiry Date is overdue (we can unban the user.), or the expiryDate.
  chatChannelGetBanExpiryDate(alreadyBannedUser: ChatChannelBannedOrMutedUser): Date {
    const now = new Date();
    if (alreadyBannedUser.expiryDate < now) //if ban/mute has expired.
      return undefined;
    return alreadyBannedUser.expiryDate;
  }

  // sets the expiryDate of the ban or mute sanction on a user. 
  private chatChannelSetExpiryDateForBannedOrMutedUser(bannedOrMutedUser: ChatChannelBannedOrMutedUser, durationInSeconds: number) {
    const now = new Date();
    now.setSeconds(now.getSeconds() + durationInSeconds);
    bannedOrMutedUser.expiryDate = now;
  }

  /* **************************************************************************************** */
  /* SERVICE FUNCTIONS SECTION                                                                */
  /* **************************************************************************************** */

  //OK
  //create a chatChannel, login becomes owner, and becomes an admin as well.
  async createOneChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    await User.findOne({
      where: { login: query['login'] }
    })
      .catch(error => {
        //console.log('\x1b[36m%s\x1b[0m', error)
        addInternalServerErrorToResponseObj(responseObj);
      })
      .then(async creator => {
        if (!creator) {
          addErrorToResponseObj(responseObj, "Wrong login, chatChannel creator does not exist.", 0);
          return JSON.stringify(responseObj);
        }
        let newChannel = new ChatChannel();
        newChannel.owner = creator;
        newChannel.admins = [creator];
        if (query.hasOwnProperty('password'))
          newChannel.password = await hashPassword(query['password']) as string;
        else
          newChannel.password = '';
        if (query.hasOwnProperty('chatChannelName') && query['chatChannelName'] !== '')
          newChannel.chatChannelName = query['chatChannelName'];
        if (query['privateChatChannel'] === 'true')
          newChannel.privateChatChannel = true;
        else
          newChannel.privateChatChannel = false;
        await ChatChannel.save(newChannel)
          .then(res => {
            //console.log(`new chatChannel created for ${query['login']} (id = ${res.id})`)
            //adding the id of the newly created channel in the data.
            const chatChannelObj = {
              id: res.id,
              createdAt: res.createdAt,
              chatChannelName: res.chatChannelName,
              privateChatChannel: res.privateChatChannel,
              owner: res.owner,
            }
            responseObj.data = chatChannelObj;
          })
          .catch((error) => {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
          });
      })
    return JSON.stringify(responseObj);
  }


  //TODO: make sure the deletion of a User is OK... and doest not leave unused relations...
  //mayby use softdelete?

  /* TODO
    - maybe give each chatChannel a different name... at least a unique random number? or the one of the chatChannel itself?
      - if doing so, the chatChannel name 
  */

  //OK
  //NOTE: only the Owner of the chatChannel can delete its property.
  //deletes a chatChannel from the database, and "cascades on delete" its relations in owner, admins, and guests tables.
  async deleteOneChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      const chatChannel = await this.getChatChannelFromItsIdAndItsOwnerLogin(query['idChannel'], query['login'],);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, "invalid chatChannel owner login, or invalid chatChannelId", 0);
        return JSON.stringify(responseObj)
      }
      await ChatChannel.remove(chatChannel);
      //console.log(`${query['login']} (Owner) deleted chatChannel ${query['idChannel']} (channel id).`);
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //This function is called when we want to add another user into a given private chatChannel.
  //If the login is the owner, no password is required at all.
  //If the login is someone else, if the chatChannel is protected by a password, it must be provided.
  async inviteMemberToPrivateChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      const chatChannel = await this.getChatChannelFromItsIdWithPassword(query['idChannel']);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, "invalid chatChannelId", 0);
        return JSON.stringify(responseObj)
      }
      const anyMember = await this.chatChannelGetAnyUserByLoginInChatChannelById(chatChannel.id, query['login']);
      if (!anyMember) {
        addErrorToResponseObj(responseObj, "login isn't a member of given chatChannel.", 1);
        return JSON.stringify(responseObj)
      }
      const invited = await this.getUserFromItsLogin(query['loginTarget']);
      if (!invited) {
        addErrorToResponseObj(responseObj, "loginTarget doesnt exist.", 2);
        return JSON.stringify(responseObj)
      }
      if (await this.chatChannelUserAlreadyBelongsToChannel(chatChannel, invited) === true) {
        addErrorToResponseObj(responseObj, `loginTarget ${query['loginTarget']} is already a member of chatChannel`, 3);
        return JSON.stringify(responseObj)
      }
      const alreadyBannedUser: ChatChannelBannedOrMutedUser = await this.chatChannelGetBannedOrMutedUserByLoginInChatChannelById(chatChannel.id, query['loginTarget'], ChatChannelUserState.BANNED);
      if (alreadyBannedUser) {
        let banExpiryDate: Date = this.chatChannelGetBanExpiryDate(alreadyBannedUser);
        if (banExpiryDate) { //statement targets other than undefined variable.
          addErrorToResponseObj(responseObj, `${banExpiryDate}`, 4);
          return JSON.stringify(responseObj)
        }
        await ChatChannelBannedOrMutedUser.remove(alreadyBannedUser);
      }
      //TODO: password encryption...
      if (chatChannel.owner.login != query['login'] &&
        chatChannel.hasOwnProperty('password') && !await comparePassword(chatChannel.password, "")) {
        //console.log("should get here if no password is provided and not the owner.")
        if (!query.hasOwnProperty('password') || query['password'] === '') {
          //return a error, password required.
          addErrorToResponseObj(responseObj, "password required.", 5);
          return JSON.stringify(responseObj)
        }
        else if (!await comparePassword(chatChannel.password, query['password'])) {
          //return another type of error. wrong password.
          addErrorToResponseObj(responseObj, "wrong password.", 6);
          return JSON.stringify(responseObj)
        }
      }
      await ChatChannel.createQueryBuilder()
        .relation(ChatChannel, "guests")
        .of(chatChannel)
        .add(invited);
      //console.log(`${query['loginTarget']} is added by ${query['login']} to chatChannel ${query['idChannel']} as a guest.`);
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK
  /*
  **  unregister from a chatChannel.
  **  3 cases:
  **    - login === owner, the entire chatChannel is removed.
  **    - login === an admin, the admin is removed from the admins list.
  **    - login === an guest, the guest is removed from the guests list.
  */
  async quitChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      const chatChannel = await this.getChatChannelFromItsIdWithPassword(query['idChannel']);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, "invalid chatChannelId", 0);
        return JSON.stringify(responseObj)
      }
      if (query['login'] === chatChannel.owner.login) { //remove the entire chatChannel.
        await ChatChannel.remove(chatChannel);
        //console.log(`${query['login']} (Owner) left and deleted chatChannel ${query['idChannel']} (channel id).`);
        return JSON.stringify(responseObj);
      }
      const anyuser = await this.chatChannelGetAnyUserByLoginInChatChannelById(chatChannel.id, query['login']);
      if (!anyuser) {
        addErrorToResponseObj(responseObj, "login isn't a member of given chatChannel.", 1);
        return JSON.stringify(responseObj)
      }
      await this.chatChannelRemoveUser(chatChannel, anyuser);
      //console.log(`${query['login']} is leaving chatChannel ${query['idChannel']}.`);
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK
  //register to a chatChannel, as a guest user (not an admin or owner).
  async joinPublicChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      const chatChannel = await this.getChatChannelFromItsIdWithPassword(query['idChannel']);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, "invalid chatChannelId", 0);
        return JSON.stringify(responseObj)
      }
      if (chatChannel.privateChatChannel) {
        addErrorToResponseObj(responseObj, "Private chatChannel, you must be added by a member.", 1);
        return JSON.stringify(responseObj)
      }
      const user = await this.getUserFromItsLogin(query['login']);
      if (!user) {
        addErrorToResponseObj(responseObj, "login doesnt exist.", 2);
        return JSON.stringify(responseObj)
      }
      if (await this.chatChannelUserAlreadyBelongsToChannel(chatChannel, user) === true) {
        addErrorToResponseObj(responseObj, `login ${query['login']} is already a member of chatChannel`, 3);
        return JSON.stringify(responseObj)
      }
      const alreadyBannedUser: ChatChannelBannedOrMutedUser = await this.chatChannelGetBannedOrMutedUserByLoginInChatChannelById(chatChannel.id, query['login'], ChatChannelUserState.BANNED);
      if (alreadyBannedUser) {
        let banExpiryDate: Date = this.chatChannelGetBanExpiryDate(alreadyBannedUser);
        if (banExpiryDate) { //statement targets other than undefined variable.
          addErrorToResponseObj(responseObj, `${banExpiryDate}`, 4);
          return JSON.stringify(responseObj)
        }
        await ChatChannelBannedOrMutedUser.remove(alreadyBannedUser);
      }
      if (chatChannel.hasOwnProperty('password') && !await comparePassword(chatChannel.password, "")) {
        if (!query.hasOwnProperty('password') || query['password'] === '') {
          addErrorToResponseObj(responseObj, "password required.", 5);
          return JSON.stringify(responseObj)
        }
        else if (!await comparePassword(chatChannel.password, query['password'])) {
          addErrorToResponseObj(responseObj, "wrong password.", 6);
          return JSON.stringify(responseObj)
        }
      }
      await ChatChannel.createQueryBuilder()
        .relation(ChatChannel, "guests")
        .of(chatChannel)
        .add(user);
      //console.log(`${query['login']} is joining chatChannel ${query['idChannel']} as a guest.`);
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK.
  //note: the owner is not part of the data.
  async getListOfPublicChatChannels(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
        addErrorToResponseObj(responseObj, "The limit or the offset parameter cannot be a negative number.", 0);
        return JSON.stringify(responseObj)
      }
      const listPublicChatChannels = await ChatChannel.createQueryBuilder("chatChannels")
        .leftJoinAndSelect("chatChannels.owner", "owner")
        .where('chatChannels.privateChatChannel = :falseBool')
        .setParameter('falseBool', 'false')
        .offset(query['offset'])
        .limit(query['limit'])
        .getMany();
      //console.log(`list of Public chatChannels requested by login ${query['login']}: `);
      //listPublicChatChannels.forEach(element => console.log(element));
      responseObj.data = listPublicChatChannels;
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK.
  //It gets a list of the chatChannel created by the login parameter.
  //The privacy enum in query allows us to discriminate between private, public or both chatChannels created by login.
  async getListOfChatChannelsAsAnyMember(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      if (query.hasOwnProperty('limit') && +query['limit'] < 0) {
        addErrorToResponseObj(responseObj, "The limit parameter cannot be a negative number.", 0);
        return JSON.stringify(responseObj)
      }
      if (!query.hasOwnProperty('privacy') || (query['privacy'] != 'ANY' && query['privacy'] != 'PRIVATE' && query['privacy'] != 'PUBLIC')) {
        addErrorToResponseObj(responseObj, "the privacy is required, and should take a value between 'ANY', 'PRIVATE' or 'PUBLIC'.", 1);
        return JSON.stringify(responseObj)
      }
      let listJoinedChatChannels: ChatChannel[];
      if (query['privacy'] === 'PRIVATE') {
        listJoinedChatChannels = await ChatChannel.createQueryBuilder("chatChannels")
          .leftJoin("chatChannels.admins", "admins")
          .leftJoin("chatChannels.guests", "guests")
          .leftJoinAndSelect("chatChannels.owner", "owner")
          .where('chatChannels.privateChatChannel = :privacy', { privacy: true })
          .andWhere(new Brackets(qb => {
            qb.where('admins.login = :loginAdmin', { loginAdmin: query['login'] })
              .orWhere('guests.login = :loginGuest', { loginGuest: query['login'] })
          }))
          .limit(query['limit'])
          .getMany();
        //console.log(`list of Private chatChannels login ${query['login']} belongs to: `);
        //listJoinedChatChannels.forEach(element => //console.log(element));
      }
      else if (query['privacy'] === 'PUBLIC') {
        listJoinedChatChannels = await ChatChannel.createQueryBuilder("chatChannels")
          .leftJoin("chatChannels.admins", "admins")
          .leftJoin("chatChannels.guests", "guests")
          .leftJoinAndSelect("chatChannels.owner", "owner")
          .where('chatChannels.privateChatChannel = :privacy', { privacy: false })
          .andWhere(new Brackets(qb => {
            qb.where('admins.login = :loginAdmin', { loginAdmin: query['login'] })
              .orWhere('guests.login = :loginGuest', { loginGuest: query['login'] })
          }))
          .limit(query['limit'])
          .getMany();
        //console.log(`list of Public chatChannels login ${query['login']} belongs to: `);
        //listJoinedChatChannels.forEach(element => //console.log(element));
      }
      else {
        listJoinedChatChannels = await ChatChannel.createQueryBuilder("chatChannels")
          .leftJoin("chatChannels.admins", "admins")
          .leftJoin("chatChannels.guests", "guests")
          .leftJoinAndSelect("chatChannels.owner", "owner")
          .where('admins.login = :loginAdmin', { loginAdmin: query['login'] })
          .orWhere('guests.login = :loginGuest', { loginGuest: query['login'] })
          .limit(query['limit'])
          .getMany();
        //console.log(`list of all chatChannels login ${query['login']} belongs to: `);
        //listJoinedChatChannels.forEach(element => //console.log(element));
      }
      responseObj.data = listJoinedChatChannels;
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK.
  //It gets a list of the chatChannel created by the login parameter.
  //The privacy enum in query allows us to discriminate between private, public or both chatChannels created by login.
  async getListOfChatChannelsAsOwner(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      if (query.hasOwnProperty('limit') && +query['limit'] < 0) {
        addErrorToResponseObj(responseObj, "The limit parameter cannot be a negative number.", 0);
        return JSON.stringify(responseObj)
      }
      if (!query.hasOwnProperty('privacy') || (query['privacy'] != 'ANY' && query['privacy'] != 'PRIVATE' && query['privacy'] != 'PUBLIC')) {
        addErrorToResponseObj(responseObj, "the privacy is required, and should take a value between 'ANY', 'PRIVATE' or 'PUBLIC'.", 1);
        return JSON.stringify(responseObj)
      }
      let listOwnedChatChannels: ChatChannel[];
      if (query['privacy'] === 'PRIVATE') {
        listOwnedChatChannels = await ChatChannel.createQueryBuilder("chatChannels")
          .leftJoinAndSelect("chatChannels.owner", "owner")
          .where('chatChannels.privateChatChannel = :privacy', { privacy: true })
          .andWhere('owner.login = :loginOwner', { loginOwner: query['login'] })
          .limit(query['limit'])
          .getMany();
        //console.log(`list of Private chatChannels owned by login ${query['login']}: `);
        //listOwnedChatChannels.forEach(element => //console.log(element));
      }
      else if (query['privacy'] === 'PUBLIC') {
        listOwnedChatChannels = await ChatChannel.createQueryBuilder("chatChannels")
          .leftJoinAndSelect("chatChannels.owner", "owner")
          .where('chatChannels.privateChatChannel = :privacy', { privacy: false })
          .andWhere('owner.login = :loginOwner', { loginOwner: query['login'] })
          .limit(query['limit'])
          .getMany();
        //console.log(`list of Public chatChannels owned by login ${query['login']}: `);
        // listOwnedChatChannels.forEach(element => //console.log(element));
      }
      else {
        listOwnedChatChannels = await ChatChannel.createQueryBuilder("chatChannels")
          .leftJoinAndSelect("chatChannels.owner", "owner")
          .where('owner.login = :loginOwner', { loginOwner: query['login'] })
          .limit(query['limit'])
          .getMany();
        //console.log(`list of all chatChannels owned by login ${query['login']}: `);
        //listOwnedChatChannels.forEach(element => //console.log(element));
      }
      responseObj.data = listOwnedChatChannels;
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK
  //returns a list of admins(including the owner) within the given chatChannel.
  //note: the operation can only be performed by a user that belongs within the
  //chatChannel already.
  //the limit parameter, if not provided at all, is not taken into account.
  async getListOfAdminProfilesInChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      if (query.hasOwnProperty('limit') && +query['limit'] < 0) {
        addErrorToResponseObj(responseObj, "The limit parameter cannot be a negative number.", 0);
        return JSON.stringify(responseObj)
      }
      const userBelongingToChatChannel = await this.chatChannelGetAnyUserByLoginInChatChannelById(query['idChannel'], query['login']);
      if (!userBelongingToChatChannel) {
        addErrorToResponseObj(responseObj, `Invalid idChannel(${query['idChannel']}), or login(${query['login']}) doesnt belong to chatChannel.`, 1);
        return JSON.stringify(responseObj)
      }
      const listAdmins = await User.createQueryBuilder('user')
        .leftJoinAndSelect(ChatChannel, "chatChannelOwner", "chatChannelOwner.owner = user.id")
        .leftJoin("user.chatChannelsAsAdmin", "chatChannelsAsAdmin")
        .where('chatChannelOwner.id = :idChannel1', { idChannel1: query['idChannel'] })
        .orWhere('chatChannelsAsAdmin.id = :idChannel2', { idChannel2: query['idChannel'] })
        .limit(query['limit'])
        .getMany();
      responseObj.data = listAdmins;
      //console.log(`listAdmins requested by login ${query['login']} on chatChannel with id ${query['idChannel']}: `);
      //listAdmins.forEach(element => //console.log(element));
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK
  //returns a list of guests-only (no admins and owner) profiles within the given chatChannel
  //note: the operation can only be performed by a user that belongs within the
  //chatChannel already.
  //the limit and the offset parameters, if not provided at all, are not taken into account.
  async getListOfGuestProfilesInChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
        addErrorToResponseObj(responseObj, "The limit or the offset parameter cannot be a negative number.", 0);
        return JSON.stringify(responseObj)
      }
      const userBelongingToChatChannel = await this.chatChannelGetAnyUserByLoginInChatChannelById(query['idChannel'], query['login']);
      if (!userBelongingToChatChannel) {
        addErrorToResponseObj(responseObj, `Invalid idChannel(${query['idChannel']}), or login(${query['login']}) doesnt belong to chatChannel.`, 1);
        return JSON.stringify(responseObj)
      }
      const listGuests = await User.createQueryBuilder('user')
        .leftJoin("user.chatChannelsAsGuest", "chatChannelsAsGuest")
        .where('chatChannelsAsGuest.id = :idChannel3', { idChannel3: query['idChannel'] })
        .offset(query['offset'])
        .limit(query['limit'])
        .getMany();
      responseObj.data = listGuests;
      //console.log(`listGuests requested by login ${query['login']} on chatChannel with id ${query['idChannel']}: `);
      // listGuests.forEach(element => //console.log(element));
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK
  //NOTE: the login asking the list must belong within the given chatChannel
  //Returns a list in which any user is mixed (owner, admins and guests).
  //the limit and the offset parameters, if not provided at all, are not taken into account.
  async getListOfAllProfilesInChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
        addErrorToResponseObj(responseObj, "The limit or the offset parameter cannot be a negative number.", 0);
        return JSON.stringify(responseObj)
      }
      const userBelongingToChatChannel = await this.chatChannelGetAnyUserByLoginInChatChannelById(query['idChannel'], query['login']);
      if (!userBelongingToChatChannel) {
        addErrorToResponseObj(responseObj, `Invalid idChannel(${query['idChannel']}), or login(${query['login']}) doesnt belong to chatChannel.`, 1);
        return JSON.stringify(responseObj)
      }
      const listAllUsers = await User.createQueryBuilder('user')
        .leftJoinAndSelect(ChatChannel, "chatChannelOwner", "chatChannelOwner.owner = user.id")
        .leftJoin("user.chatChannelsAsAdmin", "chatChannelsAsAdmin")
        .leftJoin("user.chatChannelsAsGuest", "chatChannelsAsGuest")
        .where('chatChannelOwner.id = :idChannel1', { idChannel1: query['idChannel'] })
        .orWhere('chatChannelsAsAdmin.id = :idChannel2', { idChannel2: query['idChannel'] })
        .orWhere('chatChannelsAsGuest.id = :idChannel3', { idChannel3: query['idChannel'] })
        .offset(query['offset'])
        .limit(query['limit'])
        .getMany();
      responseObj.data = listAllUsers;
      //console.log(`listAllUsers requested by login ${query['login']} on chatChannel with id ${query['idChannel']}: `);
      //listAllUsers.forEach(element => //console.log(element));
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }


  //OK
  //note: if the login is not the owner, it wont work.
  async changePasswordInChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      const chatChannel = await this.getChatChannelFromItsIdAndItsOwnerLogin(query['idChannel'], query['login'],);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, "invalid chatChannel owner login, or invalid chatChannelId", 0);
        return JSON.stringify(responseObj)
      }
      await this.chatChannelRepository.save({
        id: chatChannel.id,
        password: await hashPassword(query['password']) as string,
      });
      //console.log(`${query['login']} (Owner) is updating password for chatChannel ${query['idChannel']}`);
    }
    catch (err) {
      //console.log('\x1b[36m%s\x1b[0m', err)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK
  /*
  ** This operation can only be performed by the owner of the chatChannel.
  ** The new admin to add must already belong to the chatChannel as a guest user.
  ** If the guest user is muted at the time, it is unmuted.
  ** If the new admin login is currently banned, it means it is not part of the chatChannel anymore.
  ** It cannot be added until it joins the chatChannel as a guest in the first place.
  */
  async adminAddToChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      //TODO replace flmarsil by query['login'] !!
      //const chatChannel = await this.getChatChannelFromItsIdAndItsOwnerLogin(query['idChannel'], "flmarsil",);
      const chatChannel = await this.getChatChannelFromItsIdAndItsOwnerLogin(query['idChannel'], query['login']);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, "invalid chatChannel owner login, or invalid chatChannelId", 0);
        return JSON.stringify(responseObj)
      }
      const adminAlreadyAdmin: User = await this.chatChannelGetAdminUserByLoginInChatChannelById(chatChannel.id, query['loginNewAdmin']);
      if (adminAlreadyAdmin) {
        addErrorToResponseObj(responseObj, `loginNewAdmin ${query['loginNewAdmin']} is already an amdin in the chatChannel.`, 1);
        return JSON.stringify(responseObj)
      }
      const guestToBecomeAdmin: User = await this.chatChannelGetGuestUserByLoginInChatChannelById(chatChannel.id, query['loginNewAdmin']);
      if (!guestToBecomeAdmin) {
        addErrorToResponseObj(responseObj, `loginNewAdmin ${query['loginNewAdmin']} must already belong to chatChannel as a guest in order to become an admin.`, 2);
        return JSON.stringify(responseObj)
      }
      const mutedUser = await this.chatChannelGetBannedOrMutedUserByLoginInChatChannelById(chatChannel.id, query['loginNewAdmin'], ChatChannelUserState.MUTED);
      if (mutedUser) { //if user to become admin is muted, it is unmuted.
        //console.log(`mute sanction REMOVED on User ${query['loginNewAdmin']} as user becomes admin.`)
        await ChatChannelBannedOrMutedUser.remove(mutedUser);
      }
      await ChatChannel
        .createQueryBuilder()
        .relation(ChatChannel, "admins")
        .of(chatChannel)
        .add(guestToBecomeAdmin);
      //console.log(`New admin ${query['loginNewAdmin']} is added into chatChannel ${query['idChannel']} as an admin.`)
      await ChatChannel
        .createQueryBuilder()
        .relation(ChatChannel, "guests")
        .of(chatChannel)
        .remove(guestToBecomeAdmin);
      //console.log(` -> New admin ${query['loginNewAdmin']} is removed from chatChannel ${query['idChannel']} as a guest.`)
    }
    catch (err) {
      //console.log('\x1b[36m%s\x1b[0m', err)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK
  //Note: it can only be performed by the owner
  //Note: it is not possible to remove an admin that is also the owner...
  //Note: if the admin to be deleted doesnt exist in chatChannel, error.
  //Note: When an admin is deleted, it should re-become a simple guest.
  async adminRemoveFromChatChannel(responseObj: ResponseOBJ, query): Promise<string> {
    try {
      const chatChannel = await this.getChatChannelFromItsIdAndItsOwnerLogin(query['idChannel'], query['login'],);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, "Invalid chatChannel owner login, or invalid chatChannelId.", 0);
        return JSON.stringify(responseObj)
      }
      const admin = await this.chatChannelGetAdminUserByLoginInChatChannelById(chatChannel.id, query['loginTargetAdmin']);
      if (!admin) {
        addErrorToResponseObj(responseObj, `loginTargetAdmin ${query['loginTargetAdmin']} does not match any existing Admin.`, 1);
        return JSON.stringify(responseObj)
      }
      if (admin.login === chatChannel.owner.login) {
        addErrorToResponseObj(responseObj, `loginTargetAdmin ${query['loginTargetAdmin']} is the owner of the chatChannel.`, 2);
        return JSON.stringify(responseObj)
      }
      await ChatChannel
        .createQueryBuilder()
        .relation(ChatChannel, "admins")
        .of(chatChannel)
        .remove(admin);
      await ChatChannel
        .createQueryBuilder()
        .relation(ChatChannel, "guests")
        .of(chatChannel)
        .add(admin);
      //console.log(`Admin ${query['loginTargetAdmin']} is removed from chatChannel ${query['idChannel']} (id).`)
      //console.log(` >> Admin ${query['loginTargetAdmin']} was re - added as a simple guest.`)
    }
    catch (err) {
      //console.log('\x1b[36m%s\x1b[0m', err)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  /* **************************************************************************************** */
  /* BAN OR MUTE SECTION                                                                      */
  /* **************************************************************************************** */

  //OK
  //returns the user (instance of class User), that is to be banned or muted.
  //sets error 6, 7
  async chatChannelFindUserToBanOrMute(responseObj: ResponseOBJ, query, chatChannel: ChatChannel, admin: User): Promise<User> {
    let userToBanOrMute: User;
    if (admin.id === chatChannel.owner.id) { //we can then ban any other admin/guest in the chatChannel.
      userToBanOrMute = await this.chatChannelGetAnyUserByLoginInChatChannelById(query['idChannel'], query['loginTarget']);
      if (!userToBanOrMute) {
        addErrorToResponseObj(responseObj, `loginTarget ${query['loginTarget']} does not match any existing admin / guest in chatChannel`, 6);
      }
    }
    else { //if the Admin is not the owner, just an admin, we can only ban a regular guest.
      userToBanOrMute = await this.chatChannelGetGuestUserByLoginInChatChannelById(query['idChannel'], query['loginTarget']);
      if (!userToBanOrMute) {
        addErrorToResponseObj(responseObj, `loginTarget ${query['loginTarget']} does not match any existing guest in chatChannel`, 7);
      }
    }
    return userToBanOrMute;
  }

  //OK
  /*
  ** This function will take the care of the situation where we want to ban/mute an user, and it was already muted or banned.
  ** - case1: If the user was already banned. error
  ** - case2: If the user was already muted and we try to mute it, error.
  ** - case3: If the user was already muted and we try to ban it, then we shift its state from mute to ban, and we remove it from the
  ** chatChannel's list of guests/admins.
  ** Sets error status 4, 5.
  */
  async chatChannelHandleAlreadyBannedOrMutedUser(responseObj: ResponseOBJ, query, banOrMute: ChatChannelUserState,
    chatChannelBannedOrMutedUser: ChatChannelBannedOrMutedUser, chatChannel: ChatChannel, admin: User): Promise<string> {
    //case1:
    if (chatChannelBannedOrMutedUser.state === ChatChannelUserState.BANNED) {
      addErrorToResponseObj(responseObj, `loginTarget ${query['loginTarget']} is already banned.Cannot mute / ban user anymore.`, 4);
      return JSON.stringify(responseObj)
    }
    //case2:
    if (chatChannelBannedOrMutedUser.state === ChatChannelUserState.MUTED && banOrMute === ChatChannelUserState.MUTED) {
      addErrorToResponseObj(responseObj, `loginTarget ${query['loginTarget']} is already muted.Cannot mute user anymore.`, 5);
      return JSON.stringify(responseObj)
    }
    //case3:
    //remove him with call to removebanned user...
    let userToBan: User = await this.chatChannelFindUserToBanOrMute(responseObj, query, chatChannel, admin);
    if (!userToBan) { //sets error 6 or 7.
      return JSON.stringify(responseObj)
    }
    await this.chatChannelRemoveUser(chatChannel, userToBan);
    chatChannelBannedOrMutedUser.state = ChatChannelUserState.BANNED;
    this.chatChannelSetExpiryDateForBannedOrMutedUser(chatChannelBannedOrMutedUser, +query['durationInSeconds']);
    //console.log(`Admin ${query['loginAdmin']} has UPDATED from mute to ban on User ${query['loginTarget']} in chatChannel ${query['idChannel']} `)
    await ChatChannelBannedOrMutedUser.save(chatChannelBannedOrMutedUser);
    return JSON.stringify(responseObj)
  }

  //OK
  /*
  ** This operation can only be performed by an administrator of the chat channel.
  ** The owner of the chat channel cannot be banned/muted within its chat channel at all.
  ** An admin cannot ban/mute another admin, only the owner can ban/mute another admin.
  ** NOTE: Whoever is muted, remains in the chatChannel, but shuts the fuck up for the set duration.
  ** NOTE: Whoever is banned, gets totally removed from the given chatChannel and cannot re-enter it for the set duration.
  **    An admin that is banned gets removed from the list of admins. A Guest that is banned gets removed from the guests list.
  ** NOTE: if an User was previously muted, and is then banned on top of that, its muted state gets shifted to banned, and it is
  **    removed from the chatChannel as well.
  */
  //TESTS: OK when deleting a chatChannel all the created ban or mutes are deleted in cascade. update functions as well.
  async chatChannelBanOrMuteUserInChannel(responseObj: ResponseOBJ, query, banOrMute: ChatChannelUserState): Promise<string> {
    try {
      if (query['login'] === query['loginTarget']) {
        addErrorToResponseObj(responseObj, `An user cannot ban or mute himself`, 0);
        return JSON.stringify(responseObj)
      }
      const chatChannel: ChatChannel = await this.getChatChannelFromItsIdWithPassword(query['idChannel']);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, `idChannel ${query['idChannel']} does not exist.`, 1);
        return JSON.stringify(responseObj)
      }
      if (query['loginTarget'] === chatChannel.owner.login) {
        addErrorToResponseObj(responseObj, `loginTarget ${query['loginTarget']} is the owner of the chatChannel`, 2);
        return JSON.stringify(responseObj)
      }
      const admin: User = await this.chatChannelGetAdminUserByLoginInChatChannelById(query['idChannel'], query['login']);
      if (!admin) {
        addErrorToResponseObj(responseObj, `logindAdmin ${query['login']} is not an admin of this chatChannel.`, 3);
        return JSON.stringify(responseObj)
      }
      let chatChannelBannedOrMutedUser: ChatChannelBannedOrMutedUser = await this.chatChannelGetEitherBannedOrMutedUserByLoginInChatChannelById(query['idChannel'], query['loginTarget']);
      //enter if means we previously have muted/banned the user from the chatChannel.
      if (chatChannelBannedOrMutedUser) //sets error 4, 5 if problem.
        return await this.chatChannelHandleAlreadyBannedOrMutedUser(responseObj, query, banOrMute, chatChannelBannedOrMutedUser, chatChannel, admin);
      //getting here means that the user is getting muted or banned for the first time. We create a mute/ban row in table.
      let userToBanOrMute: User = await this.chatChannelFindUserToBanOrMute(responseObj, query, chatChannel, admin);
      if (!userToBanOrMute) { //sets error 6, 7 if problem.
        return JSON.stringify(responseObj)
      }
      chatChannelBannedOrMutedUser = new ChatChannelBannedOrMutedUser();
      chatChannelBannedOrMutedUser.chatChannel = chatChannel;
      chatChannelBannedOrMutedUser.user = userToBanOrMute;
      chatChannelBannedOrMutedUser.state = banOrMute;
      chatChannelBannedOrMutedUser.userLogin = userToBanOrMute.login;
      this.chatChannelSetExpiryDateForBannedOrMutedUser(chatChannelBannedOrMutedUser, +query['durationInSeconds']);
      if (banOrMute === ChatChannelUserState.BANNED) { //we ban, and we remove the user if existing in table.
        await this.chatChannelRemoveUser(chatChannel, userToBanOrMute);
      }
      await ChatChannelBannedOrMutedUser.save(chatChannelBannedOrMutedUser); //add row in the bannedOrMuted table.
      //console.log(`Admin ${query['login']} has banned(and removed) or muted User ${query['loginTarget']} from chatChannel ${query['idChannel']} for ${query['durationInSeconds']} seconds`)
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error);
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }

  //OK
  //this function acts either for the mute mechanism or the ban mechanism.
  //all it does is finding a row in table ChatChannelBannedOrMutedUsers, and deletes it.
  async chatChannelUndoBanOrMuteOnUserInChannel(responseObj: ResponseOBJ, query, banOrMute: ChatChannelUserState): Promise<string> {
    try {
      if (query['login'] === query['loginTarget']) {
        addErrorToResponseObj(responseObj, `An user cannot unban or unmute himself`, 0);
        return JSON.stringify(responseObj)
      }
      const chatChannel: ChatChannel = await this.getChatChannelFromItsIdWithPassword(query['idChannel']);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, `idChannel ${query['idChannel']} does not exist.`, 1);
        return JSON.stringify(responseObj)
      }
      if (query['loginTarget'] === chatChannel.owner.login) {
        addErrorToResponseObj(responseObj, `loginTarget ${query['loginTarget']} is the owner of the chatChannel`, 2);
        return JSON.stringify(responseObj)
      }
      const admin: User = await this.chatChannelGetAdminUserByLoginInChatChannelById(query['idChannel'], query['login']);
      if (!admin) {
        addErrorToResponseObj(responseObj, `logindAdmin ${query['login']} is not an admin of this chatChannel.`, 3);
        return JSON.stringify(responseObj)
      }
      let chatChannelBannedOrMutedUser: ChatChannelBannedOrMutedUser =
        await this.chatChannelGetBannedOrMutedUserByLoginInChatChannelById(query['idChannel'], query['loginTarget'], banOrMute);
      if (!chatChannelBannedOrMutedUser) {
        addErrorToResponseObj(responseObj, `loginTarget ${query['loginTarget']} wasnt banned or muted(at least not anymore).`, 4);
        return JSON.stringify(responseObj)
      }
      //getting here means we want to unban and the user was banned, vice versa for the unmute / muted.
      if (banOrMute === ChatChannelUserState.BANNED) {

      } else if (admin.id === chatChannel.owner.id) {

      } else { //if the Admin is not the owner, just an admin, we can only unmute a regular guest.
        const amdinToNotUnmute = await this.chatChannelGetAdminUserByLoginInChatChannelById(chatChannel.id, query['loginTarget']);
        if (amdinToNotUnmute) { //we cannot unmute the target.
          addErrorToResponseObj(responseObj, `Admin ${query['login']} cannot unmute another admin(${query['loginTarget']}).`, 5);
          return JSON.stringify(responseObj)
        }
        //console.log(`Admin ${query['login']} has REMOVED mute sanction on User ${query['loginTarget']} in chatChannel ${query['idChannel']}`)
      }
      await ChatChannelBannedOrMutedUser.remove(chatChannelBannedOrMutedUser);
      return JSON.stringify(responseObj)
    }
    catch (error) {
      //console.log('\x1b[36m%s\x1b[0m', error);
      addInternalServerErrorToResponseObj(responseObj);
    }
  }

  //returns a list of the banned or the muted users in a given chatChannel.
  private async chatChannelGetListBannedOrMutedUsersInChatChannelById(idChannel: number, state: ChatChannelUserState): Promise<ChatChannelBannedOrMutedUser[]> {
    return await ChatChannelBannedOrMutedUser.createQueryBuilder('chatChannelBannedOrMutedUser')
      .leftJoin("chatChannelBannedOrMutedUser.chatChannel", "chatChannel")
      .where('chatChannel.id = :idChannel', { idChannel: idChannel })
      .andWhere('chatChannelBannedOrMutedUser.state = :state', { state: state })
      .getMany();
  }

  //returns a list of the banned or muted users within a given chatChannel Id.
  //The login making the request must be an admin of the chatChannel (or the owner)
  // errors: 0: wrong channel id
  //        1: not an admin.
  async getListOfBannedUserInChannel(responseObj: ResponseOBJ, query, banOrMute: ChatChannelUserState): Promise<string> {
    try {
      const chatChannel = await this.getChatChannelFromItsIdWithPassword(query['idChannel']);
      if (!chatChannel) {
        addErrorToResponseObj(responseObj, "invalid chatChannelId", 0);
        return JSON.stringify(responseObj)
      }
      const admin: User = await this.chatChannelGetAdminUserByLoginInChatChannelById(query['idChannel'], query['login']);
      if (!admin) {
        addErrorToResponseObj(responseObj, `loginAdmin ${query['login']} is not an admin of this chatChannel id ${query['idChannel']}`, 1);
        return JSON.stringify(responseObj)
      }
      const listBannedUsers: ChatChannelBannedOrMutedUser[] = await this.chatChannelGetListBannedOrMutedUsersInChatChannelById(query['idChannel'], banOrMute);
      responseObj.data = listBannedUsers;
    }
    catch (err) {
      //console.log('\x1b[36m%s\x1b[0m', err)
      addInternalServerErrorToResponseObj(responseObj);
    }
    return JSON.stringify(responseObj);
  }
}
