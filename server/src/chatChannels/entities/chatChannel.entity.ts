import { Entity, JoinTable, JoinColumn, CreateDateColumn, OneToMany, ManyToOne, ManyToMany, PrimaryGeneratedColumn, Column, BaseEntity, } from 'typeorm';
import { User } from "../../users/entities/user.entity"
import { ChatChannelBannedOrMutedUser } from './chatChannelBannedOrMutedUser.entity';

@Entity({ name: "chatChannels" })
export class ChatChannel extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: "chat channel" })
    chatChannelName: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column({ default: false })
    privateChatChannel: boolean;

    //note: the select false, makes the property not appear using createQueryBuilder,
    //unless we use the addSelect() statement before the where().
    @Column({ select: false })
    password: string;

    //Only one owner at a time. The owner is the creator of the channel.
    //And an owner can possess multiple chatChannels at the same time.
    @ManyToOne((type) => User, (user) => user.id,
        {
            onDelete: 'CASCADE', //if the owner user is deleted, this chatChannel is deleted.
        })
    @JoinColumn({ name: "owner_userId", })
    owner: User;

    //A chatChannel can have multiple admins (at least one: the owner).
    //admins are added by the owner of the chatChannel.
    //Each admin can potentially be the admin of multiple chatChannels.
    @ManyToMany((type) => User, (user) => user.chatChannelsAsAdmin)
    @JoinTable(
        {
            name: "chatChannelId_adminId", // table name for the junction table of this relation
            joinColumn: {
                name: "chatChannelId",
                referencedColumnName: "id"
            },
            inverseJoinColumn: {
                name: "admin_userId",
                referencedColumnName: "id"
            }
        }
    )
    admins: User[];

    //A chatChannel can have multiple simpleUsers.
    //Each simpleUsers can potentially belong to multiple chatChannels at the same time.
    @ManyToMany((type) => User, (user) => user.chatChannelsAsGuest,
        {
            onDelete: 'CASCADE'
        })
    @JoinTable(
        {
            name: "chatChannelId_guestId", // table name for the junction table of this relation
            joinColumn: {
                name: "chatChannelId",
                referencedColumnName: "id"
            },
            inverseJoinColumn: {
                name: "guest_userId",
                referencedColumnName: "id"
            }
        }
    )
    guests: User[];

    //for the bannedOrMuted relation: it is an intermediate entity.
    @OneToMany((type) => ChatChannelBannedOrMutedUser, (chatChannelBannedOrMutedUser) => chatChannelBannedOrMutedUser.chatChannel,)
    chatChannelBannedOrMutedUsers!: ChatChannelBannedOrMutedUser[];
}
