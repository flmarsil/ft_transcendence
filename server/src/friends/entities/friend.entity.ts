import { type } from 'os';
import { Entity, CreateDateColumn, PrimaryColumn, UpdateDateColumn, JoinTable, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, ManyToMany, OneToOne, OneToMany } from 'typeorm';
import { User } from "../../users/entities/user.entity"


export enum FriendState {
    PENDING_FIRST_TO_SECOND = 'pending_first_to_second',
    PENDING_SECOND_TO_FIRST = 'pending_second_to_first',
    FRIEND = 'friend',
    BLOCK_BY_FIRST = 'block_by_first',
    BLOCK_BY_SECOND = 'block_by_second',
}


@Entity({ name: "friends" })
export class Friend extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'enum',
        enum: FriendState,
    })
    state: FriendState;

    @CreateDateColumn()
    created_at: Date;

    //TODO ajouter dans users.
    @UpdateDateColumn()
    updated_at: Date;

    @PrimaryColumn()
    user_first_login: string;

    @PrimaryColumn()
    user_second_login: string;


    @ManyToMany(type => User, user => user.login)
    @JoinTable()
    users: User[];
}