import { Controller, Get, Post, Query, Delete } from '@nestjs/common';
import { FriendsService } from './friends.service';
import { initResponse } from '../utils-dto/utils.dto';

@Controller('/api/v1')
export class FriendsController {
  constructor(private readonly friendsService: FriendsService) { }

  @Get('/friends')
  async getListOfFriends(@Query() query): Promise<string> {
    return await this.friendsService.getListOfFriends(initResponse(query), query);
  }

  @Get('/friends/request')
  async getContactRequest(@Query() query): Promise<string> {
    return await this.friendsService.getContactRequest(initResponse(query), query);
  }

  @Get('/friends/search')
  async searchFriend(@Query() query): Promise<string> {
    return await this.friendsService.searchFriend(initResponse(query), query);
  }

  @Post('/friend')
  async setContactUser(@Query() query): Promise<string> {
    return await this.friendsService.setContactUser(initResponse(query), query);
  }

  @Delete('/friend')
  async deleteContactUser(@Query() query): Promise<string> {
    return await this.friendsService.deleteContactUser(initResponse(query), query);
  }

  @Post('/friends/accept')
  async acceptFriend(@Query() query): Promise<string> {
    return await this.friendsService.acceptFriend(initResponse(query), query);
  }

  @Post('/friends/refuse')
  async refuseFriend(@Query() query): Promise<string> {
    return await this.friendsService.refuseFriend(initResponse(query), query);
  }

}
