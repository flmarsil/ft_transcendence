import { Injectable, } from '@nestjs/common';
import { Repository, Brackets } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ResponseOBJ, ResponseStatus, isUserNameValid, addErrorToResponseObj, addInternalServerErrorToResponseObj, } from '../utils-dto/utils.dto';
import { GameHistory, GameHistoryGameType } from './entities/gameHistory.entity';
import { User } from '../users/entities/user.entity'

@Injectable()
export class GameHistoryService {

    constructor(
        @InjectRepository(GameHistory)
        private gameHistoryRepository: Repository<GameHistory>,
    ) { }

    /* **************************************************************************************** */
    /* PRIVATE HELPER FUNCTIONS SECTION                                                         */
    /* **************************************************************************************** */

    //this function can throw if the database access goes wrong
    //returns Promise<void> or Promise<User>
    private async getUserFromItsUsername(username: string): Promise<User> {
        return await User.findOne({ where: { username: username } });
    }

    //to get the list of Winned games against another specific opponent.
    private async gameHistoryForUsernameGetListWinsWithOpponent(query, user: User, userOpponent: User, gameType: GameHistoryGameType): Promise<GameHistory[]> {
        let listWins: GameHistory[];
        if (query.hasOwnProperty('gameType') && query['gameType'] !== 'ANYGAME') {
            listWins = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoinAndSelect("gameHistory.loser", "loser")
                .leftJoin("gameHistory.winner", "winner")
                .where("winner.username = :usernameWinner", { usernameWinner: user.username })
                .andWhere("gameHistory.gameType = :gameType", { gameType: gameType })
                .andWhere("loser.username = :usernameLoser", { usernameLoser: userOpponent.username })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list wins on ${gameType} against ${userOpponent.username}:\x1b[0m\n`, listWins);
        }
        else {
            listWins = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoinAndSelect("gameHistory.loser", "loser")
                .leftJoin("gameHistory.winner", "winner")
                .where("winner.username = :usernameWinner", { usernameWinner: user.username })
                .andWhere("loser.username = :usernameLoser", { usernameLoser: userOpponent.username })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list wins on any game type against ${userOpponent.username}:\x1b[0m\n`, listWins);
        }
        return listWins;
    }

    //to get a list of winned games without opponents.
    private async gameHistoryForUsernameGetListWinsNoOpponent(query, user: User, gameType: GameHistoryGameType): Promise<GameHistory[]> {
        let listWins: GameHistory[];
        if (query.hasOwnProperty('gameType') && query['gameType'] !== 'ANYGAME') {
            listWins = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoinAndSelect("gameHistory.loser", "loser")
                .leftJoin("gameHistory.winner", "winner")
                .where("winner.username = :usernameWinner", { usernameWinner: user.username })
                .andWhere("gameHistory.gameType = :gameType", { gameType: gameType })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list wins on ${gameType}:\x1b[0m\n`, listWins);
        }
        else {
            listWins = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoinAndSelect("gameHistory.loser", "loser")
                .leftJoin("gameHistory.winner", "winner")
                .where("winner.username = :usernameWinner", { usernameWinner: user.username })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list wins on any game type:\x1b[0m\n`, listWins);
        }
        return listWins;
    }

    //to get the list of Lost games against another specific opponent.
    private async gameHistoryForUsernameGetListLosesWithOpponent(query, user: User, userOpponent: User, gameType: GameHistoryGameType): Promise<GameHistory[]> {
        let listLoses: GameHistory[];
        if (query.hasOwnProperty('gameType') && query['gameType'] !== 'ANYGAME') {
            listLoses = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoin("gameHistory.loser", "loser")
                .leftJoinAndSelect("gameHistory.winner", "winner")
                .where("loser.username = :usernameLoser", { usernameLoser: user.username })
                .andWhere("gameHistory.gameType = :gameType", { gameType: gameType })
                .andWhere("winner.username = :usernameWinner", { usernameWinner: userOpponent.username })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list loses on ${gameType} against ${userOpponent.username}:\x1b[0m\n`, listLoses);
        }
        else {
            listLoses = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoin("gameHistory.loser", "loser")
                .leftJoinAndSelect("gameHistory.winner", "winner")
                .where("loser.username = :usernameLoser", { usernameLoser: user.username })
                .andWhere("winner.username = :usernameWinner", { usernameWinner: userOpponent.username })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list loses on any game type against ${userOpponent.username}:\x1b[0m\n`, listLoses);
        }
        return listLoses;
    }

    //to get a list of Lost games without opponents.
    private async gameHistoryForUsernameGetListLosesNoOpponent(query, user: User, gameType: GameHistoryGameType): Promise<GameHistory[]> {
        let listLoses: GameHistory[];
        if (query.hasOwnProperty('gameType') && query['gameType'] !== 'ANYGAME') {
            listLoses = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoin("gameHistory.loser", "loser")
                .leftJoinAndSelect("gameHistory.winner", "winner")
                .where("loser.username = :usernameLoser", { usernameLoser: user.username })
                .andWhere("gameHistory.gameType = :gameType", { gameType: gameType })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list loses on ${gameType}:\x1b[0m\n`, listLoses);
        }
        else {
            listLoses = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoin("gameHistory.loser", "loser")
                .leftJoinAndSelect("gameHistory.winner", "winner")
                .where("loser.username = :usernameLoser", { usernameLoser: user.username })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list loses on any game type:\x1b[0m\n`, listLoses);
        }
        return listLoses;
    }

    //to get the list of any games against another specific opponent.
    private async gameHistoryForUsernameGetListWithOpponent(query, user: User, userOpponent: User, gameType: GameHistoryGameType): Promise<GameHistory[]> {
        let listAnyOutcome: GameHistory[];
        if (query.hasOwnProperty('gameType') && query['gameType'] !== 'ANYGAME') {
            listAnyOutcome = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoinAndSelect("gameHistory.loser", "loser")
                .leftJoinAndSelect("gameHistory.winner", "winner")
                .where("gameHistory.gameType = :gameType", { gameType: gameType })
                .andWhere(new Brackets(qb => {
                    qb.where(new Brackets(qb1 => {
                        qb1.where("loser.username = :username1", { username1: user.username })
                            .andWhere("winner.username = :usernameOpponent1", { usernameOpponent1: userOpponent.username })
                    }))
                        .orWhere(new Brackets(qb2 => {
                            qb2.where("winner.username = :username2", { username2: user.username })
                                .andWhere("loser.username = :usernameOpponent2", { usernameOpponent2: userOpponent.username })
                        }))
                }))
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list any outcome on ${gameType} against ${userOpponent.username}:\x1b[0m\n`, listAnyOutcome);
        }
        else {
            listAnyOutcome = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoinAndSelect("gameHistory.loser", "loser")
                .leftJoinAndSelect("gameHistory.winner", "winner")
                .where(new Brackets(qb => {
                    qb.where(new Brackets(qb1 => {
                        qb1.where("loser.username = :username1", { username1: user.username })
                            .andWhere("winner.username = :usernameOpponent1", { usernameOpponent1: userOpponent.username })
                    }))
                        .orWhere(new Brackets(qb2 => {
                            qb2.where("winner.username = :username2", { username2: user.username })
                                .andWhere("loser.username = :usernameOpponent2", { usernameOpponent2: userOpponent.username })
                        }))
                }))
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list any outcome on any game type against ${userOpponent.username}:\x1b[0m\n`, listAnyOutcome);
        }
        return listAnyOutcome;
    }

    //to get a list of any games without any specific opponents.
    private async gameHistoryForUsernameGetListNoOpponent(query, user: User, gameType: GameHistoryGameType): Promise<GameHistory[]> {
        let listAnyOutcome: GameHistory[];
        if (query.hasOwnProperty('gameType') && query['gameType'] !== 'ANYGAME') {
            listAnyOutcome = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoinAndSelect("gameHistory.loser", "loser")
                .leftJoinAndSelect("gameHistory.winner", "winner")
                .where("gameHistory.gameType = :gameType", { gameType: gameType })
                .andWhere(new Brackets(qb => {
                    qb.where("loser.username = :username", { username: user.username })
                        .orWhere("winner.username = :username", { username: user.username })
                }))
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list any outcome on ${gameType}:\x1b[0m\n`, listAnyOutcome);
        }
        else {
            listAnyOutcome = await GameHistory.createQueryBuilder("gameHistory")
                .leftJoinAndSelect("gameHistory.loser", "loser")
                .leftJoinAndSelect("gameHistory.winner", "winner")
                .where("loser.username = :username", { username: user.username })
                .orWhere("winner.username = :username", { username: user.username })
                .orderBy("gameHistory.id", 'DESC')
                .offset(query['offset'])
                .limit(query['limit'])
                .getMany();
            //console.log(`\x1b[35mfor user ${user.username}, list any outcome on any game:\x1b[0m\n`, listAnyOutcome);
        }
        return listAnyOutcome;
    }
    /* **************************************************************************************** */
    /* SERVICE FUNCTIONS SECTION                                                                */
    /* **************************************************************************************** */

    //error 0, 1, 2 already consumed in controller.
    //note: in the result array, each index contains only the loser (winner is always the username parameter).
    async getListWinnedGamesForUsername(responseObj: ResponseOBJ, query): Promise<string> {
        try {
            let listWins: GameHistory[];
            const user = await this.getUserFromItsUsername(query['username']);
            if (!user) {
                addErrorToResponseObj(responseObj, "username doesnt exist.", 3);
                return JSON.stringify(responseObj);
            }
            if (query.hasOwnProperty('usernameOpponent')) {
                const userOpponent = await this.getUserFromItsUsername(query['usernameOpponent']);
                if (!userOpponent) {
                    addErrorToResponseObj(responseObj, "usernameOpponent doesnt exist.", 4);
                    return JSON.stringify(responseObj);
                }
                listWins = await this.gameHistoryForUsernameGetListWinsWithOpponent(query, user, userOpponent, query['gameType']);
            }
            else
                listWins = await this.gameHistoryForUsernameGetListWinsNoOpponent(query, user, query['gameType']);
            responseObj.data = listWins;
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    //error 0, 1, 2 already consumed in controller.
    //note: in the result array, each index contains only the winner (loser is always the username parameter).
    async getListLostGamesForUsername(responseObj: ResponseOBJ, query): Promise<string> {
        try {
            let listLoses: GameHistory[];
            const user = await this.getUserFromItsUsername(query['username']);
            if (!user) {
                addErrorToResponseObj(responseObj, "username doesnt exist.", 3);
                return JSON.stringify(responseObj);
            }
            if (query.hasOwnProperty('usernameOpponent')) {
                const userOpponent = await this.getUserFromItsUsername(query['usernameOpponent']);
                if (!userOpponent) {
                    addErrorToResponseObj(responseObj, "usernameOpponent doesnt exist.", 4);
                    return JSON.stringify(responseObj);
                }
                listLoses = await this.gameHistoryForUsernameGetListLosesWithOpponent(query, user, userOpponent, query['gameType']);
            }
            else
                listLoses = await this.gameHistoryForUsernameGetListLosesNoOpponent(query, user, query['gameType']);
            responseObj.data = listLoses;
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    //error 0, 1, 2 already consumed in controller.
    //note: in the result array, each index contains both the winner and the loser.
    async getListGamesForUsername(responseObj: ResponseOBJ, query): Promise<string> {
        try {
            let listLoses: GameHistory[];
            const user = await this.getUserFromItsUsername(query['username']);
            if (!user) {
                addErrorToResponseObj(responseObj, "username doesnt exist.", 3);
                return JSON.stringify(responseObj);
            }
            if (query.hasOwnProperty('usernameOpponent')) {
                const userOpponent = await this.getUserFromItsUsername(query['usernameOpponent']);
                if (!userOpponent) {
                    addErrorToResponseObj(responseObj, "usernameOpponent doesnt exist.", 4);
                    return JSON.stringify(responseObj);
                }
                listLoses = await this.gameHistoryForUsernameGetListWithOpponent(query, user, userOpponent, query['gameType']);
            }
            else
                listLoses = await this.gameHistoryForUsernameGetListNoOpponent(query, user, query['gameType']);
            responseObj.data = listLoses;
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    //error 0, 1 already consumed in controller.
    async getListLastPlayedGamesForAnyone(responseObj: ResponseOBJ, query): Promise<string> {
        try {
            let listGameHistoryAllTime: GameHistory[];
            if (query.hasOwnProperty('gameType') && query['gameType'] !== 'ANYGAME') {
                listGameHistoryAllTime = await GameHistory.createQueryBuilder("gameHistory")
                    .leftJoinAndSelect("gameHistory.loser", "loser")
                    .leftJoinAndSelect("gameHistory.winner", "winner")
                    .where("gameHistory.gameType = :gameType", { gameType: query['gameType'] })
                    .orderBy("gameHistory.id", 'DESC')
                    .offset(query['offset'])
                    .limit(query['limit'])
                    .getMany();
                //console.log(`\x1b[35mlist any outcome on game type ${query['gameType']}, since all time:\x1b[0m\n`, listGameHistoryAllTime);
            }
            else {
                listGameHistoryAllTime = await GameHistory.createQueryBuilder("gameHistory")
                    .leftJoinAndSelect("gameHistory.loser", "loser")
                    .leftJoinAndSelect("gameHistory.winner", "winner")
                    .orderBy("gameHistory.id", 'DESC')
                    .offset(query['offset'])
                    .limit(query['limit'])
                    .getMany();
                //console.log(`\x1b[35mlist any outcome on any game since all time:\x1b[0m\n`, listGameHistoryAllTime);

            }
            responseObj.data = listGameHistoryAllTime;
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error)
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

    //TODO REMOVE ME
    async gameHistorySetTmp(responseObj: ResponseOBJ, query): Promise<string> {
        try {
            if (!query.hasOwnProperty('winner') || !query.hasOwnProperty('loser')) {
                addErrorToResponseObj(responseObj, "winner and loser param required", 0);
                return JSON.stringify(responseObj);
            }
            if (query.hasOwnProperty('gameType') && (query['gameType'] !== GameHistoryGameType.DUELGAME && query['gameType'] !== GameHistoryGameType.LADDERGAME)) {
                addErrorToResponseObj(responseObj, "enum gameType, must be either ['LADDERGAME', 'DUELGAME']", 1);
                return JSON.stringify(responseObj);
            }
            const winner: User = await this.getUserFromItsUsername(query['winner']);
            if (!winner) {
                addErrorToResponseObj(responseObj, "winner doesnt exist in db.", 2);
                return JSON.stringify(responseObj);
            }
            const loser: User = await this.getUserFromItsUsername(query['loser']);
            if (!loser) {
                addErrorToResponseObj(responseObj, "loser doesnt exist in db.", 3);
                return JSON.stringify(responseObj);
            }
            const gameHistory = new GameHistory();
            gameHistory.winnerLogin = winner.login;
            gameHistory.winner = winner;
            gameHistory.loserLogin = loser.login;
            gameHistory.loser = loser;
            gameHistory.dateEndOfGame = new Date();
            gameHistory.gameDurationInSeconds = 42; //should be start - now.
            gameHistory.scoreLoser = 1;
            gameHistory.scoreWinner = 3;
            if (query.hasOwnProperty('gameType'))
                gameHistory.gameType = query['gameType'];
            GameHistory.save(gameHistory);
            //console.log(`game history created: winner = ${winner.username}, loser = ${loser.username}.`)
        }
        catch (error) {
            //console.log('\x1b[36m%s\x1b[0m', error);
            addInternalServerErrorToResponseObj(responseObj);
        }
        return JSON.stringify(responseObj);
    }

}