import { Controller, Get, Post, Query, Res, } from '@nestjs/common';
import { addErrorToResponseObj, initResponse, ResponseOBJ, } from '../utils-dto/utils.dto';
import { GameHistoryGameType } from './entities/gameHistory.entity';
import { GameHistoryService } from './gameHistory.service';

@Controller('api/v1')
export class GameHistoryController {
    constructor(private readonly gameHistoryService: GameHistoryService) { }

    //sets error 0, 1, 2
    @Get('gameHistory/wins')
    async getListWinnedGamesForUsername(@Query() query): Promise<string> {
        const responseObj: ResponseOBJ = initResponse(query);
        if (!query.hasOwnProperty('username')) {
            addErrorToResponseObj(responseObj, "username parameter missing.", 0);
            return JSON.stringify(responseObj);
        }
        if (!query.hasOwnProperty('gameType') || (query['gameType'] !== 'ANYGAME' && query['gameType'] !== GameHistoryGameType.LADDERGAME && query['gameType'] !== GameHistoryGameType.DUELGAME)) {
            addErrorToResponseObj(responseObj, "enum gameType required: ['LADDERGAME', 'DUELGAME', 'ANYGAME']", 1);
            return JSON.stringify(responseObj);
        }
        if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
            addErrorToResponseObj(responseObj, "limit or offset parameter cannot be a negativ number.", 2);
            return JSON.stringify(responseObj);
        }
        return await this.gameHistoryService.getListWinnedGamesForUsername(initResponse(query), query);
    }

    //sets error 0, 1, 2
    @Get('gameHistory/loses')
    async getListLostGamesForUsername(@Query() query): Promise<string> {
        const responseObj: ResponseOBJ = initResponse(query);
        if (!query.hasOwnProperty('username')) {
            addErrorToResponseObj(responseObj, "username parameter missing.", 0);
            return JSON.stringify(responseObj);
        }
        if (!query.hasOwnProperty('gameType') || (query['gameType'] !== 'ANYGAME' && query['gameType'] !== GameHistoryGameType.LADDERGAME && query['gameType'] !== GameHistoryGameType.DUELGAME)) {
            addErrorToResponseObj(responseObj, "enum gameType required: ['LADDERGAME', 'DUELGAME', 'ANYGAME']", 1);
            return JSON.stringify(responseObj);
        }
        if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
            addErrorToResponseObj(responseObj, "limit or offset parameter cannot be a negativ number.", 2);
            return JSON.stringify(responseObj);
        }
        return await this.gameHistoryService.getListLostGamesForUsername(initResponse(query), query);
    }

    //sets error 0, 1, 2
    @Get('gameHistory/any')
    async getListGamesForUsername(@Query() query): Promise<string> {
        const responseObj: ResponseOBJ = initResponse(query);
        if (!query.hasOwnProperty('username')) {
            addErrorToResponseObj(responseObj, "username parameter missing.", 0);
            return JSON.stringify(responseObj);
        }
        if (!query.hasOwnProperty('gameType') || (query['gameType'] !== 'ANYGAME' && query['gameType'] !== GameHistoryGameType.LADDERGAME && query['gameType'] !== GameHistoryGameType.DUELGAME)) {
            addErrorToResponseObj(responseObj, "enum gameType required: ['LADDERGAME', 'DUELGAME', 'ANYGAME']", 1);
            return JSON.stringify(responseObj);
        }
        if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
            addErrorToResponseObj(responseObj, "limit or offset parameter cannot be a negativ number.", 2);
            return JSON.stringify(responseObj);
        }
        return await this.gameHistoryService.getListGamesForUsername(initResponse(query), query);
    }

    //sets errors 0, 1
    @Get('gameHistories/mainBoard')
    async getListLastPlayedGamesForAnyone(@Query() query): Promise<string> {
        const responseObj: ResponseOBJ = initResponse(query);
        if (!query.hasOwnProperty('gameType') || (query['gameType'] !== 'ANYGAME' && query['gameType'] !== GameHistoryGameType.LADDERGAME && query['gameType'] !== GameHistoryGameType.DUELGAME)) {
            addErrorToResponseObj(responseObj, "enum gameType required: ['LADDERGAME', 'DUELGAME', 'ANYGAME']", 0);
            return JSON.stringify(responseObj);
        }
        if ((query.hasOwnProperty('limit') && +query['limit'] < 0) || (query.hasOwnProperty('offset') && +query['offset'] < 0)) {
            addErrorToResponseObj(responseObj, "limit or offset parameter cannot be a negativ number.", 1);
            return JSON.stringify(responseObj);
        }
        return this.gameHistoryService.getListLastPlayedGamesForAnyone(initResponse(query), query);
    }

    //TODO remove me, this will be done by the websocket...
    @Post('gameHistory/')
    async gameHistorySetTmp(@Query() query): Promise<string> {
        return await this.gameHistoryService.gameHistorySetTmp(initResponse(query), query);
    }
}