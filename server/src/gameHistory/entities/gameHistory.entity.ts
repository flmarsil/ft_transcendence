import { Entity, CreateDateColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, Column, BaseEntity, ManyToMany } from 'typeorm';
import { User } from '../../users/entities/user.entity'

export enum GameHistoryGameType {
    LADDERGAME = 'LADDERGAME',
    DUELGAME = 'DUELGAME',
}

@Entity({ name: "gameHistories" })
export class GameHistory extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @CreateDateColumn()
    dateEndOfGame: Date

    @Column()
    gameDurationInSeconds: number

    @Column({ default: GameHistoryGameType.LADDERGAME })
    gameType: GameHistoryGameType;

    @Column({ unique: false, default: '' })
    gameMap: string;

    @Column()
    scoreLoser: number

    @Column()
    scoreWinner: number

    @Column()
    loserLogin: string //stays even if the loser is deleted from the database.

    @ManyToOne((type) => User, (user) => user.gameHistoryAsLoser,
        {
            onDelete: 'SET NULL',
            nullable: true
        })
    loser: User;

    @Column()
    winnerLogin: string //persists even if the winner is deleted from the db.

    @ManyToOne((type) => User, (user) => user.gameHistoryAsWinner,
        {
            onDelete: 'SET NULL',
            nullable: true
        })
    winner: User;
}
