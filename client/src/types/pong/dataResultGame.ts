import dataPlayer from "./dataPlayer";

interface dataResultGame {
  gameId: string;
  loser: dataPlayer;
  time: Date;
  winner: dataPlayer;
}

export default dataResultGame;
