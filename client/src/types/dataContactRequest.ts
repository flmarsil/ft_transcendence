interface dataContactRequest {
  avatar: string;
  created_at: Date;
  id: number;
  username: string;
}

export default dataContactRequest;
