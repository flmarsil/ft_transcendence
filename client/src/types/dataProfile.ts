interface dataProfile {
  id: number;
  // login: string;
  isblocked: boolean;
  isfriend: boolean;
  username: string;
  avatar: string;
  wins: number;
  loses: number;
  ratio: number;
  xp: number;
  lvl: number;
  status: number;
  playingCurrently: boolean;
  createdAt: string;
}

export default dataProfile;
