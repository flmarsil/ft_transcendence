import dataProfile from "./dataProfile";

interface dataGameInProgress {
  id: number;
  username_player1: string;
  username_player2: string;
  gameID: string;
  mapName: string;
  createdAt: Date;
}

export default dataGameInProgress;
