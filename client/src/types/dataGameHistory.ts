import dataProfile from "./dataProfile";

interface gameHistory {
  id: number;
  dateEndOfGame: Date;
  gameDurationInSecondes: number;
  gameType: string;
  scoreLoser: number;
  scoreWinner: number;
  loserLogin: string;
  loser: dataProfile;
  winnerLogin: string;
  winner: dataProfile;
}

export default gameHistory;
