import { computed, reactive, readonly, Component } from "vue";
import Ball from "./ball";
import Paddle from "./paddle";
import { DIRECTION } from "./direction";

let rounds = [5, 5, 3, 3, 2] as number[];
let colors = ["#1abc9c", "#2ecc71", "#3498db", "#e74c3c", "#9b59b6"];

let colorMapClassic = ["#1a233a", "#65f7c1"]; // background, paddle&ball
let colorMapBeach = ["#e8ac63", "#151166"]; // background, paddle&ball
let colorMapLight = ["#661b01", "#676b02"]; // background, paddle&ball

// dimension du canvas au niveau du back end - a modifier en conséquanece pour la conservation du ratio
let serverWidth = 1500;
let serverHeight = 1000;

export default class Pong {
  private _canvas: HTMLCanvasElement;
  private _ctx: CanvasRenderingContext2D;

  private ball: Ball;

  private player1: Paddle;
  private player2: Paddle;

  private player1_score: number;
  private player2_score: number;

  private running: boolean;
  private over: boolean;
  private timer: number;
  private round: number;
  private color: string;

  private _usernamePlayer1: string;
  private _usernamePlayer2: string;

  private _nameMap: string;
  constructor(
    newCanvas: HTMLCanvasElement,
    usernamePlayer1: string,
    usernamePlayer2: string,
    nameMap: string
  ) {
    this._canvas = newCanvas;
    this._usernamePlayer1 = usernamePlayer1;
    this._usernamePlayer2 = usernamePlayer2;
    this._nameMap = nameMap;
    this.color = "676b02";
    // this.initialize();
    // this._canvas.width = 1500;
    // this._canvas.height = 1000;
    // this._canvas.style.width = this._canvas.width + "px";
    // this._canvas.style.height = this._canvas.height + "px";
    // Get the device pixel ratio, falling back to 1.
    //var dpr = window.devicePixelRatio || 1;
    // Get the size of the canvas in CSS pixels.
    //var rect = this._canvas.getBoundingClientRect();
    // Give the canvas pixel dimensions of their CSS
    // size * the device pixel ratio.
    //this._canvas.width = rect.width * dpr;
    //this._canvas.height = rect.height * dpr;
    this._ctx = this._canvas.getContext("2d") as CanvasRenderingContext2D;
    // Scale all drawing operations by the dpr, so you
    // don't have to worry about the difference.
    //this._ctx.scale(dpr, dpr);

    this.player1 = new Paddle(
      18,
      70,
      150,
      this._canvas.height / 2 - 35,
      10,
      DIRECTION.LEFT,
      0
    );
    this.player2 = new Paddle(
      18,
      70,
      this._canvas.width - 200,
      this._canvas.height / 2 - 35,
      10,
      DIRECTION.RIGHT,
      0
    );

    this.player1_score = 0;
    this.player2_score = 0;

    this.ball = new Ball(
      18,
      9, // a changer = vitesse de la balle (speed)
      this._canvas.width / 2 - 9,
      this._canvas.height / 2 - 9
    );

    this.running = this.over = false;
    this.timer = this.round = 0;

    // background canvas color
    if (this._nameMap === "classic") {
      this.color = colorMapClassic[0];
    } else if (this._nameMap === "beach-volley") {
      this.color = colorMapBeach[0];
    } else if (this._nameMap === "light") {
      this.color = colorMapLight[0];
    }
  }

  turnDelayIsOver = () => {
    return new Date().getTime() - this.timer >= 1000;
  };

  // resizeCanvas(width: number, height: number) {
  //   //console.log("in resizeCanvas: OK");
  //   // this._canvas.width = width;
  //   // this._canvas.height = height;
  // }

  saveProportion(
    varServer: number,
    varClient: number,
    toResize: number
  ): number {
    let result = (varServer - varClient) / varServer;
    let dif = toResize * result;
    return toResize - dif;
  }

  newframe = (player1: any, player2: any, ball: any) => {
    ////console.log("new position player :", player1)

    this.player1.setFrame(
      this.saveProportion(serverWidth, this._canvas.width, player1.w),
      this.saveProportion(serverHeight, this._canvas.height, player1.h),
      this.saveProportion(serverWidth, this._canvas.width, player1.x),
      this.saveProportion(serverHeight, this._canvas.height, player1.y),
      player1.speed
    );
    this.player2.setFrame(
      this.saveProportion(serverWidth, this._canvas.width, player2.w),
      this.saveProportion(serverHeight, this._canvas.height, player2.h),
      this.saveProportion(serverWidth, this._canvas.width, player2.x),
      this.saveProportion(serverHeight, this._canvas.height, player2.y),
      player2.speed
    );
    this.ball.setFrame(
      this.saveProportion(serverWidth, this._canvas.width, ball.s),
      this.saveProportion(serverWidth, this._canvas.width, ball.x),
      this.saveProportion(serverHeight, this._canvas.height, ball.y),
      ball.speed
    );
    //Pong.draw();
    if (!this.over) requestAnimationFrame(this.loop);
  };

  newScore = (player1_score: number, player2_score: number) => {
    this.player1_score = player1_score;
    this.player2_score = player2_score;
    this.draw();
    //if (!this.over) requestAnimationFrame(Pong.loop);
  };

  draw = () => {
    // Clear the Canvas
    this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);

    // Set the fill style to black
    this._ctx.fillStyle = this.color;

    // Draw the background
    this._ctx.fillRect(0, 0, this._canvas.width, this._canvas.height);

    // Set the fill style to white (For the paddles and the ball)
    if (this._nameMap === "classic") {
      this._ctx.fillStyle = colorMapClassic[1];
    } else if (this._nameMap === "beach-volley") {
      this._ctx.fillStyle = colorMapBeach[1];
    } else if (this._nameMap === "light") {
      this._ctx.fillStyle = colorMapLight[1];
    }

    // Draw the Player
    this._ctx.fillRect(
      this.player1.getX(),
      this.player1.getY(),
      this.player1.getWidth(),
      this.player1.getHeight()
    );

    // Draw the Paddle
    this._ctx.fillRect(
      this.player2.getX(),
      this.player2.getY(),
      this.player2.getWidth(),
      this.player2.getHeight()
    );

    // Draw the Ball
    if (this.turnDelayIsOver.call(this)) {
      this._ctx.fillRect(
        this.ball.getX(),
        this.ball.getY(),
        this.ball.getSize(),
        this.ball.getSize()
      );
    }

    // Draw the net (Line in the middle)
    // this._ctx.beginPath();
    // this._ctx.setLineDash([7, 15]);
    // this._ctx.moveTo(this._canvas.width / 2, this._canvas.height - 140);
    // this._ctx.lineTo(this._canvas.width / 2, 140);
    // this._ctx.lineWidth = 10;
    // this._ctx.strokeStyle = "#65f7c1";
    // this._ctx.stroke();

    // Set the default _canvas font and align it to the center
    this._ctx.font = "1rem Courier New";
    this._ctx.textAlign = "center";

    // Draw the players score (left)
    // this._ctx.fillText(
    //   this.player1_score.toString(),
    //   this._canvas.width / 2 - 300,
    //   200
    // );

    // Draw the paddles score (right)
    // this._ctx.fillText(
    //   this.player2_score.toString(),
    //   this._canvas.width / 2 + 300,
    //   200
    // );

    // Change the font size for the center score text
    // this._ctx.font = "1rem Courier New";

    // Draw the winning score (center)
    // this._ctx.fillText(
    //   "Round " + (this.round + 1),
    //   this._canvas.width / 2,
    //   35,
    //   100
    // );

    // add name player in canvas
    // this._ctx.fillText(
    //   this._usernamePlayer1,
    //   this._canvas.width / 2 - 100,
    //   this._canvas.height + 2
    // );
    // Change the font size for the center score value
    // this._ctx.font = "1rem Courier";

    // Draw the current round number
    // this._ctx.fillText(
    //   rounds[this.round].toString()
    //     ? rounds[this.round].toString()
    //     : rounds[this.round - 1].toString(),
    //   this._canvas.width / 2,
    //   100
    // );
  };

  loop = () => {
    //Pong.update();
    this.draw();
    // If the game is not over, draw the next frame.
    if (!this.over) requestAnimationFrame(this.loop);
  };
}
