import { DIRECTION } from "./direction";

// The ball object (The cube that bounces back and forth)
export default class Ball {
  private size_: number;
  private x_: number;
  private y_: number;
  private moveX_: DIRECTION;
  private moveY_: DIRECTION;
  private speed_: number;

  constructor(
    private size: number,
    private speed: number,
    private x: number,
    private y: number
  ) {
    this.size_ = this.size;
    this.speed_ = this.speed;
    this.x_ = this.x;
    this.y_ = this.y;
    this.moveX_ = DIRECTION.IDLE;
    this.moveY_ = DIRECTION.IDLE;
  }

  getSize(): number {
    return this.size_;
  }
  getX(): number {
    return this.x_;
  }
  getY(): number {
    return this.y_;
  }
  getMoveX(): DIRECTION {
    return this.moveX_;
  }
  getMoveY(): DIRECTION {
    return this.moveY_;
  }
  getSpeed(): number {
    return this.speed_;
  }

  setSize(size: number): number {
    return (this.size_ = size);
  }
  setX(x: number): number {
    return (this.x_ = x);
  }
  setY(y: number): number {
    return (this.y_ = y);
  }
  setMoveX(moveX: DIRECTION): DIRECTION {
    return (this.moveX_ = moveX);
  }
  setMoveY(moveY: DIRECTION): DIRECTION {
    return (this.moveY_ = moveY);
  }
  setSpeed(speed: number): number {
    return (this.speed_ = speed);
  }
  setFrame(size: number, x: number, y: number, speed: number) {
    this.x_ = x;
    this.y_ = y;
    this.speed_ = speed;
    this.size_ = size;
  }
}
