// The paddle object (The two lines that move up and down)

import { DIRECTION } from "./direction";

export default class Paddle {
  private width_: number;
  private height_: number;
  private x_: number;
  private y_: number;
  private speed_: number;
  private side_: DIRECTION;
  private score_: number;

  constructor(
    private width: number,
    private height: number,
    private x: number,
    private y: number,
    private speed: number,
    private side: DIRECTION,
    private score: number
  ) {
    this.width_ = this.width;
    this.height_ = this.height;
    this.speed_ = this.speed;
    this.x_ = this.x;
    this.y_ = this.y;
    this.side_ = this.side;
    this.score_ = this.score;
  }

  getWidth(): number {
    return this.width_;
  }
  getHeight(): number {
    return this.height_;
  }
  getX(): number {
    return this.x_;
  }
  getY(): number {
    return this.y_;
  }
  getSpeed(): number {
    return this.speed_;
  }
  getScore(): number {
    return this.score_;
  }
  getSide(): DIRECTION {
    return this.side_;
  }

  setWidth(width: number): number {
    return (this.width_ = width);
  }
  setHeight(height: number): number {
    return (this.height_ = height);
  }
  setX(x: number): number {
    return (this.x_ = x);
  }
  setY(y: number): number {
    return (this.y_ = y);
  }
  setSpeed(speed: number): number {
    return (this.speed_ = speed);
  }
  setScore(score: number): number {
    return (this.score_ = score);
  }
  setSide(side: DIRECTION): DIRECTION {
    return (this.side_ = side);
  }

  setFrame(width: number, height: number, x: number, y: number, speed: number) {
    this.width_ = width;
    this.height_ = height;
    this.x_ = x;
    this.y_ = y;
    this.speed_ = speed;
  }
}
