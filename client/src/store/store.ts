import { computed, reactive, readonly } from "vue";

const defaultState = {
  isAdmin: false,
  // isConnected: false,
};

const state = reactive(defaultState);

const getters = {
  getAdminStatus: () => {
    return computed(() => state.isAdmin);
  },
  // getConnectionStatus: () => {
  //   return computed(() => state.isConnected);
  // },
};

const setters = {
  setAdminStatus: (status: boolean) => {
    state.isAdmin = status;
  },
  // setConnectionStatus: (status: boolean) => {
  //   state.isConnected = status;
  // },
};

export default () => ({
  state: readonly(state),
  ...getters,
  ...setters,
});
