import { UserPublicPOV } from "./userPublicPOV";

export interface SelectedThreadTupple {
    id: Number;
    genre: ChatThreadGenre;
    thread: any;

    //true means we are selecting a person to chat with, no messages sent yet...
    newDirectMessageThreadButNothingSentYet: boolean;
    userTargetInNewDirectMessageThread: UserPublicPOV;
}

export enum ChatThreadGenre {
    UNSELECTED = 0,
    DIRECT_MESSAGE = 1,
    LIVE_CHAT = 2
}
