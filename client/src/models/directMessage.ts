export interface DirectMessage {
    id: number;

    //to see the message time and date...
    createdAt: Date;

    emitterUsername: string;

    //TODO: this must be handled.
    //note: cannot send more thann 255 characters at a time...
    message: string;
}