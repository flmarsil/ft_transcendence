import { UserPublicPOV } from "./userPublicPOV";

export interface MetaDataLiveChat {
    dataLoaded: Boolean,
    iAmOwner: Boolean,
    iAmAdmin: Boolean,
    adminUsers: UserPublicPOV[],
    guestUsers: UserPublicPOV[],
}