import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import { getAccountInfo } from "../sdk-callback/user";
// import { getIsconnected } from "@/sdk-callback/api";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/profile",
    name: "Profile",
    component: () => import("../views/Profile.vue"),
  },
  {
    path: "/chat",
    name: "Chat",
    component: () => import("../views/Chat.vue"),
    props: (route) => ({ username: route.query.username }),
  },
  {
    path: "/friends",
    name: "Friends",
    component: () => import("../views/Friends.vue"),
  },
  {
    path: "/admin",
    name: "AdminDashboard",
    component: () => import("../views/AdminDashboard.vue"),
    beforeEnter: (to, from, next) => {
      getAccountInfo().then((response) => {
        if (response.isAdmin == false) {
          next("404-error");
        } else {
          next();
        }
      });
    },
  },
  {
    path: "/:pathMatch(.*)",
    name: "404-error",
    component: () => import("../views/NotFound.vue"),
  },
  // {
  //   path: "/test",
  //   name: "Test",
  //   component: () => import("../views/Test.vue"),
  // },
];

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
});

// router.beforeEach((to, from, next) => {});

export default router;
