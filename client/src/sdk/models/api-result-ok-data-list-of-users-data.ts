/* tslint:disable */
/* eslint-disable */
/**
 * ft_transcendence_api
 * This is a ft_transcendence Api.  You can find out more about     ft_transcendence at [github](http://github.com/FouadAssani/ft_transcendence_API)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: assani@asimovcorp.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface ApiResultOkDataListOfUsersData
 */
export interface ApiResultOkDataListOfUsersData {
    /**
     * 
     * @type {string}
     * @memberof ApiResultOkDataListOfUsersData
     */
    TODO?: any;
    /**
     * 
     * @type {string}
     * @memberof ApiResultOkDataListOfUsersData
     */
    id?: any;
    /**
     * 
     * @type {string}
     * @memberof ApiResultOkDataListOfUsersData
     */
    login: any;
    /**
     * 
     * @type {string}
     * @memberof ApiResultOkDataListOfUsersData
     */
    username: any;
    /**
     * 
     * @type {string}
     * @memberof ApiResultOkDataListOfUsersData
     */
    email?: any;
    /**
     * 
     * @type {string}
     * @memberof ApiResultOkDataListOfUsersData
     */
    avatar: any;
    /**
     * 
     * @type {number}
     * @memberof ApiResultOkDataListOfUsersData
     */
    wins: any;
    /**
     * 
     * @type {number}
     * @memberof ApiResultOkDataListOfUsersData
     */
    loses: any;
    /**
     * 
     * @type {number}
     * @memberof ApiResultOkDataListOfUsersData
     */
    ratio: any;
    /**
     * 
     * @type {number}
     * @memberof ApiResultOkDataListOfUsersData
     */
    xp: any;
    /**
     * 
     * @type {string}
     * @memberof ApiResultOkDataListOfUsersData
     */
    status: ApiResultOkDataListOfUsersDataStatusEnum;
    /**
     * 
     * @type {boolean}
     * @memberof ApiResultOkDataListOfUsersData
     */
    playingCurrently: any;
    /**
     * 
     * @type {boolean}
     * @memberof ApiResultOkDataListOfUsersData
     */
    twoFactorAuth?: any;
    /**
     * 
     * @type {string}
     * @memberof ApiResultOkDataListOfUsersData
     */
    createdAt: any;
}

/**
    * @export
    * @enum {string}
    */
export enum ApiResultOkDataListOfUsersDataStatusEnum {
    OFFLINE = 'OFFLINE',
    ONLINE = 'ONLINE',
    DONOTDISTURB = 'DO_NOT_DISTURB'
}

