
let FtTranscendenceApi = require('ft_transcendence_sdk');
let gameHistory = new FtTranscendenceApi.GameHistoryApi();

export async function gameHistoriesAllTimeGetList(gameType: string, limit?: number, offset?: number) {
	let response = await gameHistory.gameHistoriesAllTimeGetList(gameType, limit, offset);
	return response.data.data;
}

export async function getListGamesForUsername(username: string, gameType: string, usernameOpponent?: string, limit?: number, offset?: number) { 
	let response = await gameHistory.getListGamesForUsername(username, gameType, usernameOpponent, limit, offset);
	return response.data.data;
}

export async function getListLostGamesForUsername(username: string, gameType: string, usernameOpponent?: string, limit?: number, offset?: number) {
	let response = await gameHistory.getListLostGamesForUsername(username, gameType, usernameOpponent, limit, offset);
	return response.data.data
}

export async function getListWinnedGamesForUsername(username: string, gameType: string, usernameOpponent?: string, limit?: number, offset?: number) {
	let response = await gameHistory.getListWinnedGamesForUsername(username, gameType, usernameOpponent, limit, offset);
	return response.data.data
}
