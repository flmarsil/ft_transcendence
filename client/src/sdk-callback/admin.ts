let FtTranscendenceApi = require('ft_transcendence_sdk');
let admin = new FtTranscendenceApi.AdminApi();


export async function getAppStats() {
	let response = await admin.getAppStats();
	return response.data.data;
}

export async function getBanListe(index: number) {
	let response = await admin.getBanListe(index);
	return response.data.data;
}

export async function getProfile(username: string) {
	let response = await admin.getProfile(username);
	return response.data.data;
}

export async function getProfilesListe(username: string, index?: number) {
	let response = await admin.getProfilesListe(username, index);
	return response.data.data;
}

export async function resetProfile(username: string) {
	let response = await admin.resetProfile(username);
	return response.data.data;
}

export async function setBan(username: string, reason: string) {
	let response = await admin.setBan(username, reason);
	return response.data.data;
}

export async function unBan(username: string) {
	let response = await admin.unBan(username);
	return response.data.data;
}