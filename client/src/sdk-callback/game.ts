let FtTranscendenceApi = require("ft_transcendence_sdk");
let apiGame = new FtTranscendenceApi.GameApi();

// To find a list of all currently played games.
export async function findGamesList(index: number) {
  let response = await apiGame.findGamesList(index);
  return response.data.data;
}

// To find a game in which username is currently playing.
export async function findGameByUsername(username: string) {
  let response = await apiGame.findGameByUsername(username);
  return response.data.data;
}

// Mettre les routes pour duel un amis
