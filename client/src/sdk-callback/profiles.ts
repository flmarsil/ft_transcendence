let FtTranscendenceApi = require("ft_transcendence_sdk");
let apiProfiles = new FtTranscendenceApi.ProfilesApi();

// block profile user
export async function blockProfile(username: string) {
  let response = await apiProfiles.blockProfile(username);
  return response.data;
}

// unblock profile user
export async function unblockProfile(username: string) {
  let response = await apiProfiles.unblockProfile(username);
  return response.data;
}

// get list of profile blocked
export async function getListOfProfileBlocked(index: number) {
  let response = await apiProfiles.getListOfProfileBlocked(index);
  return response.data;
}

// !!!!!!!!!
// get public profile from someone
export async function getProfileInfo(username: string) {
  let response = await apiProfiles.getProfileInfo(username); // recup le login via le cookie dans le localstorage
  return response.data;
}

// !!!!!!!!!
// search profile from all registred users
export async function searchProfile(username: string, index: number) {
  let response = await apiProfiles.searchProfile(username, index);
  return response.data;
}
