## Description

Group project : fassani, flmarsil, charmstr
Front : Vue js
Back : Nest js

## Installation

```bash
$ git clone git@github.com:flmarsil42/ft_transcendence.git
```

## Running the app

```bash
$ sudo docker-compose up --build
```

## Usage

```bash
http://localhost:3000/app/home  # single page app
http://localhost:3000/api       # api doc
http://localhost:3000/api/sdk   # api sdk
http://localhost:8081           # adminer (	System: PostgresSQL,
											Server: postgres,
											Username: admin,
											Password: password )

```

## dev workflow

- git pull repository.
- run command ``cd ./server && npm install`` to make sure you install locally
the depenencies into node\_modules.
- run command ``docker-compose up --build`` (only use --build on the first time).
- now you can edit files in your code editor locally, and the changes will be
mirrored within the container.
- if you want to install new modules or so, do it directly within the container:
use the command: ``docker exec -it ft_transcendence_api_1 <command here>`` 
example: ``docker exec -it backbone_container nest g resource chats`` 

